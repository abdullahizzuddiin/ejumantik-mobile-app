package gov.kemenkes.e_jumantik;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.rey.material.widget.Button;

import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.ResetPasswordPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.ResetPasswordViewOperation;

public class ResetPasswordActivity extends AppCompatActivity implements
        View.OnClickListener,
        ResetPasswordViewOperation{
    private TextInputEditText mEmailEt;
    private Button mResetButton;

    private ResetPasswordPresenterOperation mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initiatePresenter();
        initiateView();
        initiateListener();
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateView() {
        mEmailEt = (TextInputEditText) findViewById(R.id.et_reset_email);
        mResetButton = (Button) findViewById(R.id.btn_reset);
    }

    private void initiateListener() {
        mResetButton.setOnClickListener(this);
    }

    @Override
    public void showSuccessDialog(String email) {
        new AlertDialog.Builder(this)
                .setTitle("Satu Langkah Lagi!")
                .setMessage("Cek inbox email Anda ("+email+") untuk melakukan reset password")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public String getEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public void setEmailError(String message) {
        mEmailEt.setError(message);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }
}
