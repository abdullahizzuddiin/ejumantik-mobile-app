package gov.kemenkes.e_jumantik.item;

/**
 * Created by izzuddiin on 2/26/17.
 */

public class NavDrawerItem {
    private String title;
    private int icon;

    public NavDrawerItem(){}

    public NavDrawerItem(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }
}
