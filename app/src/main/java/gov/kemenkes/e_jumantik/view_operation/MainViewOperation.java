package gov.kemenkes.e_jumantik.view_operation;

/**
 * Created by izzuddiin on 3/5/17.
 */

public interface MainViewOperation {
    void navigateToMosquitoView();

    void navigateToNotificationView();

    void navigateToPointView();

    void navigateToCoverView();

    void goLogout();

    void showToast(String message);
}
