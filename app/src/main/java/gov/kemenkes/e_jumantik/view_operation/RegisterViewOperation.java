package gov.kemenkes.e_jumantik.view_operation;

/**
 * Created by izzuddiin on 2/19/17.
 */

public interface RegisterViewOperation {
    String getName();

    String getEmail();

    String getBirthPlace();

    String getBirthDate();

    String getPhoneNumber();

    String getPassword();

    String getConfPassword();

    boolean getSex();

    void setNameError(String message);

    void setEmailError(String message);

    void setBirthPlaceError(String message);

    void setBirthDateError(String message);

    void setPhoneNumberError(String message);

    void setPasswordError(String message);

    void setConfPasswordError(String message);

    void setSexError(String message);

    boolean isMaleRbChecked();

    boolean isFemaleRbChecked();

    void showDatePicker();

    void setBirthDate(String birthDate);

    void setMaleRbChecked(boolean isChecked);

    void setFemaleRbChecked(boolean isChecked);

    void navigateToCoverView();

    void showToast(String message);

    String getFacebookId();
}
