package gov.kemenkes.e_jumantik.view_operation;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;

/**
 * Created by izzuddiin on 4/1/17.
 */

public interface DetailObservingInfoViewOperation {
    void setAddress(String address);

    void setRt(String rt);

    void setRw(String rw);

    void setVillage(String village);

    void setDistrict(String district);

    void setRegency(String regency);

    void setProvince(String province);

    void setPlaceCategory(String placeCategory);

    void setPlaceType(String placeType);

    void setNumContainer(String numContainer);

    void setAnyLarvae(String anyLarvae);

    void setBeforeImage(String url, File picture);

    void setAfterImage(String url, File picture);

    void setDate(String observingDate);

    void navigateToFullscreenView(String title, String uri);

    void setContainerType(String containerType);

    void navigateToMapView(String id, LatLng latLng);
}
