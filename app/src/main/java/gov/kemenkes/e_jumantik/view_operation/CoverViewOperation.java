package gov.kemenkes.e_jumantik.view_operation;

import android.os.Bundle;

/**
 * Created by izzuddiin on 2/25/17.
 */

public interface CoverViewOperation {
    void navigateToLoginView();

    void navigateToRegisterView();

    void navigateToRegisterView(Bundle bundle);

    void showToast(String message);

    void showLoadingDialog();

    void dismissLoadingDialog();

    void navigateToMainView();
}
