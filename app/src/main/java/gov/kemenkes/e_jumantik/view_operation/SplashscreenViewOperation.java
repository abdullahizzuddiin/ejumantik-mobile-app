package gov.kemenkes.e_jumantik.view_operation;

/**
 * Created by izzuddiin on 3/31/17.
 */

public interface SplashscreenViewOperation {
    void navigateToCoverView();

    void setProgressViewValue(int percent);

    void navigateToMainView();
}
