package gov.kemenkes.e_jumantik.view_operation;

import java.util.List;

import gov.kemenkes.e_jumantik.model.Notification;

/**
 * Created by izzuddiin on 4/7/17.
 */

public interface MainNotificationViewOperation {
    void updateAdapter(List<Notification> notifications);

    void showToast(String message);
}
