package gov.kemenkes.e_jumantik.view_operation;

import java.util.List;

import gov.kemenkes.e_jumantik.model.LarvaeNest;

/**
 * Created by izzuddiin on 4/6/17.
 */

public interface MainObservingViewOperation {
    void navigateToAddObservingActivity();

    void updateAdapter(List<LarvaeNest> larvaeNests);

    void setSwipeRefreshing(boolean status);
}
