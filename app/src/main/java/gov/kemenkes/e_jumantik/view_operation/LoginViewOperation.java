package gov.kemenkes.e_jumantik.view_operation;

/**
 * Created by izzuddiin on 2/24/17.
 */

public interface LoginViewOperation {
    String getEmail();

    String getPassword();

    void setEmailError(String message);

    void setPasswordError(String message);

    void showPassword(boolean show);

    void showWrongAccountDialog();

    void showForgotPasswordDialog();

    void navigateToMainView();

    void showToast(String message);

    void navigateToResetPasswordView();
}
