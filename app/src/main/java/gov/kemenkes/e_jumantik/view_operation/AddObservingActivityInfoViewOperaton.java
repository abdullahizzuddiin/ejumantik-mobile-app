package gov.kemenkes.e_jumantik.view_operation;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;

import gov.kemenkes.e_jumantik.model.District;
import gov.kemenkes.e_jumantik.model.Province;
import gov.kemenkes.e_jumantik.model.Regency;
import gov.kemenkes.e_jumantik.model.Village;

/**
 * Created by izzuddiin on 3/28/17.
 */

public interface AddObservingActivityInfoViewOperaton    {
    void showDatePicker();

    void navigateToMainView(String instruction);

    void findImage(int requestCode);

    String getAddress();

    String getRt();

    String getRw();

    Province getProvince();

    Regency getRegency();

    District getDistrict();

    Village getVillage();

    String getCountry();

    int getPlaceType();

    int getPlaceCategory();

    int getContainerType();

    int getNumContainer();

    LatLng getSavedPosition();

    String getBeforeImageFilename();

    String getAfterImageFilename();

    String getObservingDate();

    int getAnyLarvae();

    void showToast(String message);

    void setNumContainerError(String message);

    void setDateError(String message);

    void setRwError(String message);

    void setRtError(String message);

    void setAddressError(String message);

    void showLoadingDialog();

    void dismissLoadingDialog();

    void setObservingDate(String date);

    void setAddress(String address);

    void setRt(String rt);

    void setRw(String rw);

    void setVillage(String village);

    void setDistrict(String district);

    void setRegency(String regency);

    void setProvince(String province);

    void setCountry(String country);

    void setPlaceCategory(int placeCategory);

    void setPlaceType(int placeType);

    void setNumContainer(String numContainer);

    void setContainerType(int containerType);

    void setAnyLarvae(boolean anyLarvae);

    void setBeforeImage(String url, File picture);

    void setAfterImage(String url, File picture);
}
