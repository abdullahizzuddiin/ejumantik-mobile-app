package gov.kemenkes.e_jumantik.view_operation;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by izzuddiin on 3/28/17.
 */

public interface MapViewOperation {
    void showToast(String message);

    void placeMarker(LatLng latLng);

    void moveCamera(LatLng latLng);

    void clearMap();

    void navigateToObservingInfoView(LatLng latLng);
}
