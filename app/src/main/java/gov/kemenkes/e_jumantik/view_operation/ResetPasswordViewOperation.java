package gov.kemenkes.e_jumantik.view_operation;

/**
 * Created by izzuddiin on 4/24/17.
 */

public interface ResetPasswordViewOperation {
    String getEmail();

    void setEmailError(String message);

    void showToast(String message);

    void showSuccessDialog(String email);
}
