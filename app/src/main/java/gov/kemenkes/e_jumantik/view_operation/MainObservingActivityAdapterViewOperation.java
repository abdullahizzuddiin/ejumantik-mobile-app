package gov.kemenkes.e_jumantik.view_operation;

import java.io.File;

import gov.kemenkes.e_jumantik.model.LarvaeNest;

/**
 * Created by izzuddiin on 4/4/17.
 */

public interface MainObservingActivityAdapterViewOperation {
    void shareImage(File image, LarvaeNest larvaeNest);
}
