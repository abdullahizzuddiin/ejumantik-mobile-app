package gov.kemenkes.e_jumantik;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.io.File;

import gov.kemenkes.e_jumantik.presenter.ObservingActivityPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.DetailObservingInfoPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.DetailObservingInfoViewOperation;

public class DetailObservingActivity extends AppCompatActivity implements
        DetailObservingInfoViewOperation,
        View.OnClickListener {
    private DetailObservingInfoPresenterOperation mPresenter;
    private String mLarvaeNestId;

    private FloatingActionButton mUpdateFab;

    private TextView mAddressTv;
    private TextView mRtTv;
    private TextView mRwTv;
    private TextView mVillageTv;
    private TextView mDistrictTv;
    private TextView mRegencyTv;
    private TextView mProvinceTv;
    private TextView mPlaceCategoryTv;
    private TextView mPlaceTypeTv;
    private TextView mContainerTypeTv;
    private TextView mNumContainerTv;
    private TextView mAnyLarvaeTv;
    private TextView mDateTv;
    private ImageView mBeforeIv;
    private ImageView mAfterIv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_observing);
        initiatePresenter();
        receiveExtras();
        initiateView();
        initiateData();
        initiateListener();
    }

    private void initiatePresenter() {
        mPresenter = new ObservingActivityPresenter(this, this);
    }

    private void receiveExtras() {
        mPresenter.receivedExtras(getIntent().getExtras());
    }

    private void initiateView() {
        mUpdateFab = (FloatingActionButton) findViewById(R.id.fab_update_larvae) ;
        mAddressTv = (TextView) findViewById(R.id.tv_detail_address);
        mRtTv = (TextView) findViewById(R.id.tv_detail_rt);
        mRwTv = (TextView) findViewById(R.id.tv_detail_rw);
        mVillageTv = (TextView) findViewById(R.id.tv_detail_village);
        mDistrictTv = (TextView) findViewById(R.id.tv_detail_district);
        mRegencyTv = (TextView) findViewById(R.id.tv_detail_regency);
        mProvinceTv = (TextView) findViewById(R.id.tv_detail_province);
        mPlaceCategoryTv = (TextView) findViewById(R.id.tv_detail_place_category);
        mPlaceTypeTv = (TextView) findViewById(R.id.tv_detail_place_type);
        mContainerTypeTv = (TextView) findViewById(R.id.tv_detail_container_type);
        mNumContainerTv = (TextView) findViewById(R.id.tv_detail_num_container);
        mAnyLarvaeTv = (TextView) findViewById(R.id.tv_detail_any_larvae);
        mDateTv = (TextView) findViewById(R.id.tv_detail_observing_date);
        mBeforeIv = (ImageView) findViewById(R.id.before_observe_pic_detail_iv);
        mAfterIv = (ImageView) findViewById(R.id.after_observe_pic_detail_iv);
    }

    private void initiateData() {
        mPresenter.loadData();
    }

    private void initiateListener() {
        mBeforeIv.setOnClickListener(this);
        mAfterIv.setOnClickListener(this);
        mUpdateFab.setOnClickListener(this);
    }

    @Override
    public void setAddress(String address) {
        mAddressTv.setText(address);
    }

    @Override
    public void setRt(String rt) {
        mRtTv.setText(rt);
    }

    @Override
    public void setRw(String rw) {
        mRwTv.setText(rw);
    }

    @Override
    public void setVillage(String village) {
        mVillageTv.setText(village);
    }

    @Override
    public void setDistrict(String district) {
        mDistrictTv.setText(district);
    }

    @Override
    public void setRegency(String regency) {
        mRegencyTv.setText(regency);
    }

    @Override
    public void setProvince(String province) {
        mProvinceTv.setText(province);
    }

    @Override
    public void setPlaceCategory(String placeCategory) {
        mPlaceCategoryTv.setText(placeCategory);
    }

    @Override
    public void setPlaceType(String placeType) {
        mPlaceTypeTv.setText(placeType);
    }

    @Override
    public void setContainerType(String containerType) {
        mContainerTypeTv.setText(containerType);
    }

    @Override
    public void navigateToMapView(String id, LatLng latLng) {
        Intent i = new Intent(this, AddObservingMapActivity.class);
        i.putExtra("larvae_nest_id",id);
        i.putExtra("latlng", latLng);
        startActivity(i);
    }

    @Override
    public void setNumContainer(String numContainer) {
        mNumContainerTv.setText(numContainer);
    }

    @Override
    public void setAnyLarvae(String anyLarvae) {
        mAnyLarvaeTv.setText(anyLarvae);
    }

    @Override
    public void setDate(String observingDate) {
        mDateTv.setText(observingDate);
    }

    @Override
    public void navigateToFullscreenView(String title, String uri) {
        Intent i = new Intent(DetailObservingActivity.this, FullscreenActivity.class);
        i.putExtra("title",title);
        i.putExtra("uri",uri);
        startActivity(i);
    }

    @Override
    public void setBeforeImage(String url, File file) {
        if(file == null)
            Picasso.with(this).load(url).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mBeforeIv);
        else
            Picasso.with(this).load(file).config(Bitmap.Config.RGB_565).
                fit().centerInside().into(mBeforeIv);

    }

    @Override
    public void setAfterImage(String url, File file) {
        if(file == null)
            Picasso.with(this).load(url).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mAfterIv);
        else {
            Picasso.with(this).load(file).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mAfterIv);
        }
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }
}
