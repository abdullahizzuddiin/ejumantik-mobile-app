package gov.kemenkes.e_jumantik;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rey.material.widget.ProgressView;

import java.util.Calendar;

import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.SplashscreenPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.SplashscreenViewOperation;

public class SplashscreenActivity extends AppCompatActivity implements
        SplashscreenViewOperation{

    private SplashscreenPresenterOperation mPresenter;
    private ProgressView mProgressView;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        initiatePresenter();
        initiateView();
        initiateData();
        initiateAlarm();
    }

    private void initiateView() {
        mProgressView = (ProgressView) findViewById(R.id.pv_progress);
        mProgressView.setProgress((float) 0.01);
    }

    private void initiateData() {
        mPresenter.setupData();
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateAlarm() {
        alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intentAlarm = new Intent(this, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(this, 0, intentAlarm, 0);

        // Set the alarm to start at 9:00 a.m.
        Calendar calendar = Calendar.getInstance();
//        Log.d("TAG", "MAU PASANG ALARM "+calendar.getTime().getDay()+"<-hari bulan->"+calendar.getTime().getMonth());
//        Log.d("TAG", "MAU PASANG ALARM "+calendar.getTime().getHours()+"<-jam menit->"+calendar.getTime().getMinutes());
        //how many days until sunday
        int days = Calendar.SUNDAY + (7 - calendar.get(Calendar.DAY_OF_WEEK));

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.DATE, days);
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 9);

        // setRepeating() lets you specify a precise custom interval--in this case,
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                24 * 7 * 60 * 60 * 1000, alarmIntent);

//        Log.d("TAG", "ABIS PASANG ALARM "+calendar.getTime().getDay()+"<-hari bulan->"+calendar.getTime().getMonth());
//        Log.d("TAG", "ABIS PASANG ALARM "+calendar.getTime().getHours()+"<- jam menit->"+calendar.getTime().getMinutes());
    }

    @Override
    public void navigateToCoverView() {
        Intent i = new Intent(this, CoverActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void navigateToMainView() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void setProgressViewValue(int percent) {
        mProgressView.setProgress((float) percent/100);
    }

    @Override
    public void onBackPressed() {

    }
}
