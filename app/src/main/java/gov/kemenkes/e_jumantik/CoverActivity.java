package gov.kemenkes.e_jumantik;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.rey.material.app.Dialog;
import com.shaishavgandhi.loginbuttons.FacebookButton;

import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.CoverPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.CoverViewOperation;

public class CoverActivity extends AppCompatActivity implements
        CoverViewOperation,
        View.OnClickListener {
    private Button mLoginBtn;
    private Button mRegisterBtn;
    private FacebookButton mFacebookLoginBtn;

    private CoverPresenterOperation mPresenter;

    private Dialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cover);
        initiatePresenter();
        initiateView();
        initiateListener();
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateView() {
        mLoginBtn = (Button) findViewById(R.id.btn_login_cover);
        mRegisterBtn = (Button) findViewById(R.id.btn_register_cover);
        mFacebookLoginBtn = (FacebookButton) findViewById(R.id.btn_login_facebook_cover);
    }

    private void initiateListener() {
        mLoginBtn.setOnClickListener(this);
        mRegisterBtn.setOnClickListener(this);
        mFacebookLoginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }

    @Override
    public void navigateToLoginView() {
        Intent intent = new Intent(CoverActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToRegisterView() {
        Intent intent = new Intent(CoverActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToRegisterView(Bundle bundle) {
        Intent intent = new Intent(CoverActivity.this, RegisterActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void navigateToMainView() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingDialog() {
        mLoadingDialog = new Dialog(this);
        mLoadingDialog.cancelable(false).contentView(R.layout.dialog_loading).show();
    }

    @Override
    public void dismissLoadingDialog() {
        mLoadingDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }
}
