package gov.kemenkes.e_jumantik.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.tool.PreferenceManager;

/**
 * Created by izzuddiin on 2/27/17.
 */

public class PointFragment extends Fragment {
    private TextView mTotalPointTv;
    private TextView mNumActivityTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_point, container, false);
        initiateView(layout);
        initiateData();
        return layout;
    }

    private void initiateView(View layout) {
        mTotalPointTv = (TextView) layout.findViewById(R.id.tv_num_total_point);
        mNumActivityTv = (TextView) layout.findViewById(R.id.tv_num_activity);
    }


    private void initiateData() {
        int point = PreferenceManager.getInstance(getActivity().getApplicationContext()).getPoint();
        mTotalPointTv.setText(String.valueOf(point));
        mNumActivityTv.setText(String.valueOf(point/10));
    }
}
