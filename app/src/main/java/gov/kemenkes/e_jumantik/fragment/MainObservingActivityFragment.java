package gov.kemenkes.e_jumantik.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rey.material.widget.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gov.kemenkes.e_jumantik.AddObservingMapActivity;
import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.adapter.ObservingActivityAdapter;
import gov.kemenkes.e_jumantik.model.LarvaeNest;
import gov.kemenkes.e_jumantik.presenter.ObservingActivityPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.MainObservingActivityPresenterOperation;
import gov.kemenkes.e_jumantik.tool.Utilities;
import gov.kemenkes.e_jumantik.view_operation.MainObservingActivityAdapterViewOperation;
import gov.kemenkes.e_jumantik.view_operation.MainObservingViewOperation;

/**
 * Created by izzuddiin on 2/26/17.
 */

public class MainObservingActivityFragment extends Fragment implements View.OnClickListener,
        MainObservingActivityAdapterViewOperation,
        MainObservingViewOperation, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mActivityListRv;
    private FloatingActionButton mAddObservingActivityFab;
    private ObservingActivityAdapter mAdapter;

    private MainObservingActivityPresenterOperation mPresenter;

    private String mTempInstruction;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_observing_activity, container, false);
        receiveExtras();
        initiateView(layout);
        initiatePresenter();
        initiateData();
        initiateAdapter();
        initiateListener();
        return layout;
    }

    private void receiveExtras() {
        mTempInstruction = "";
        Bundle extras = getArguments();
        if(extras != null)
            mTempInstruction = extras.getString("instruction");
    }

    private void initiatePresenter() {
        mPresenter = new ObservingActivityPresenter(getActivity().getApplicationContext(), this);
    }

    private void initiateView(View layout) {
        mRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.layout_srl);
        mRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mAddObservingActivityFab = (FloatingActionButton) layout.findViewById(R.id.fab_plus_mosquito_activity);
        mActivityListRv = (RecyclerView) layout.findViewById(R.id.rv_activity_list);
        mActivityListRv.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initiateData() {
        mPresenter.loadData();
    }

    private void initiateAdapter() {
        mAdapter = new ObservingActivityAdapter(getActivity().getBaseContext(), new ArrayList<LarvaeNest>(0));
        mAdapter.setMainView(this);
        mActivityListRv.setAdapter(mAdapter);
    }

    private void initiateListener() {
        mAddObservingActivityFab.setOnClickListener(this);
        mRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }

    @Override
    public void navigateToAddObservingActivity() {
        Intent i = new Intent(getActivity(), AddObservingMapActivity.class);
        startActivity(i);
    }

    @Override
    public void updateAdapter(List<LarvaeNest> larvaeNests) {
        mAdapter.updateObservingActivity(larvaeNests);
    }

    @Override
    public void setSwipeRefreshing(boolean status) {
        mRefreshLayout.setRefreshing(status);
    }

    @Override
    public void shareImage(File image, LarvaeNest larvaeNest) {
        Uri contentUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), "gov.kemenkes.e_jumantik.fileprovider", image);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                getActivity().getApplicationContext().getResources().getString(
                        R.string.caption_text,
                        Utilities.getPrettyDate(getActivity().getApplicationContext(), Utilities.convertDate(larvaeNest.getObservingDate()))));
        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(shareIntent);
    }

    @Override
    public void onRefresh() {
        mPresenter.loadData();
    }
}
