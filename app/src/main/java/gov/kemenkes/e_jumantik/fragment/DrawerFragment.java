package gov.kemenkes.e_jumantik.fragment;

import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.adapter.NavigationDrawerAdapter;
import gov.kemenkes.e_jumantik.item.NavDrawerItem;
import gov.kemenkes.e_jumantik.tool.PreferenceManager;
import gov.kemenkes.e_jumantik.view_operation.MainViewOperation;

/**
 * Created by izzuddiin on 2/26/17.
 */

public class DrawerFragment extends Fragment {
    private RecyclerView mNavDrawerList;
    private TextView mNameTv;

    private NavigationDrawerAdapter mAdapter;

    private String[] mTitles;
    private TypedArray mIcons;

    private View mContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    public DrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
        mIcons = getActivity().getResources().obtainTypedArray(R.array.nav_drawer_icons);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        initiateView(layout);
        initiateAdapter();

        return layout;
    }

    private void initiateView(View layout) {
        mNavDrawerList = (RecyclerView) layout.findViewById(R.id.rv_drawer_list);
        mNameTv = (TextView) layout.findViewById(R.id.nav_drawer_username);
        mNameTv.setText(this.getResources().getString(
                R.string.hello, PreferenceManager.getInstance(getActivity().getApplicationContext()).getFullname()));
    }

    private void initiateAdapter() {
        mAdapter = new NavigationDrawerAdapter(getActivity(), getData());
        mNavDrawerList.setAdapter(mAdapter);
        mNavDrawerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNavDrawerList.setHasFixedSize(true);
    }

    public void initiateToggle(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(),mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle);
        drawerLayout.post((new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        }));
    }

    public void initiateViewOperation(MainViewOperation mainView) {
        mAdapter.initiateViewOperation(mainView);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item))
            return true;

        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();

        for(int ii = 0; ii < mTitles.length; ii++) {
            NavDrawerItem item = new NavDrawerItem(mTitles[ii], mIcons.getResourceId(ii, -1));
            data.add(item);
        }
        return data;
    }
}
