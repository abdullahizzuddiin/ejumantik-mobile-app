package gov.kemenkes.e_jumantik.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.adapter.NotificationAdapter;
import gov.kemenkes.e_jumantik.model.Notification;
import gov.kemenkes.e_jumantik.presenter.NotificationPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.MainNotificationPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.MainNotificationViewOperation;

/**
 * Created by izzuddiin on 2/27/17.
 */

public class NotificationFragment extends Fragment implements MainNotificationViewOperation {
    private RecyclerView mNotificationRv;
    private NotificationAdapter mNotificationAdapter;

    private MainNotificationPresenterOperation mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_notification, container, false);
        initiatePresenter();
        initiateData();
        initiateView(layout);
        initiateAdapter();
        return layout;
    }

    private void initiateData() {
        mPresenter.loadNotification();
    }

    private void initiatePresenter() {
        mPresenter = new NotificationPresenter(getActivity().getApplicationContext(), this);
    }

    private void initiateView(View layout) {
        mNotificationRv = (RecyclerView) layout.findViewById(R.id.rv_notification_list);
        mNotificationRv.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    private void initiateAdapter() {
        mNotificationAdapter = new NotificationAdapter(getContext(),new ArrayList<Notification>(0));
        mNotificationRv.setAdapter(mNotificationAdapter);
    }

    @Override
    public void updateAdapter(List<Notification> notifications) {
        mNotificationAdapter.updateNotification(notifications);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
