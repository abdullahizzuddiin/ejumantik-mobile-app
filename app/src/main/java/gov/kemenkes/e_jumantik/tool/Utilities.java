package gov.kemenkes.e_jumantik.tool;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.rey.material.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import gov.kemenkes.e_jumantik.R;

/**
 * Created by izzuddiin on 2/19/17.
 */

public class Utilities {

    public static boolean isFormFilled(String input) {
        if(input.equals(""))
            return false;
        return true;
    }

    public static boolean isPasswordValid(String password) {
        if(password.length()<4)
            return false;

        return true;
    }

    public static boolean isEmailValid(String email) {
        String emailRegex = "^[-a-z0-9~!$%^&*_=+}{\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_]" +
                "[-a-z0-9_]*(\\.[-a-z0-9_]+)*\\." +
                "(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|" +
                "[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$";

        if(email.matches(emailRegex))
            return true;

        return false;
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {
        String phoneNumberRegex = "^(\\+[0-9]{2}|[0])[0-9]{10,12}$";

        if(phoneNumber.matches(phoneNumberRegex))
            return true;

        return false;
    }

    public static boolean isPasswordEqual(String password, String confirmationPassword) {
        if(password.equals(confirmationPassword))
            return true;
        return false;
    }

    /**
     * Convert raw json text from json file to json string
     * @param is
     * @return String json string
     */
    public static String stringFromInputStream(InputStream is) {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total;
        String line;
        try {
            total = new StringBuilder(is.available());
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return total.toString();
    }

    public static void initiateSpinnerFromJson(Spinner spinner, String json) {
        String jsonString = "";
        JSONArray mJSONArray = null;
        List<String> content = new ArrayList<>();
        content.add(spinner.getContext().getResources().getString(R.string.default_country_spn));
        try {
            jsonString = stringFromInputStream(spinner.getContext().getAssets().open(json));
            mJSONArray = new JSONArray(jsonString);
            for(int ii = 0; ii<mJSONArray.length(); ii++)
                content.add((String) mJSONArray.get(ii));

            initiateSpinnerFromList(spinner, content);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void initiateSpinnerFromList(Spinner spinner, List<?> content) {
        final ArrayAdapter<?> adapter =
                new ArrayAdapter<>(spinner.getContext(), android.R.layout.simple_spinner_item, content);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static void initiateSpinnerFromResource(Spinner spinner, int resource) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(spinner.getContext(),
                resource, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static Calendar convertDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(convertedDate);
        return cal;
    }

    public static String getPrettyDate(Context context, Calendar c) {
        String month = Constant.indonesianMonth[c.get(Calendar.MONTH)];
        return context.getResources().getString(R.string.date,c,month,c);
    }

    public static String parseSex(int sex) {
        return sex == 1? "Pria": "Wanita";
    }

    public static int parseSex(boolean sex) {
        return sex?1:0;
    }
}
