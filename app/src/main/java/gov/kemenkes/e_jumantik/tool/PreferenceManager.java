package gov.kemenkes.e_jumantik.tool;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by izzuddiin on 3/28/17.
 */

public class PreferenceManager {
    private static PreferenceManager mInstance = null;
    private static SharedPreferences.Editor mEditor;
    private static Context mContext;

    private final String PREF_NAME = "ejumantik";
    private final String FIRST_TIME_USED = "first_time";
    private final String USER_ID = "user_id";
    private final String TOKEN = "token";
    private final String FULLNAME = "fullname";
    private final String EMAIL = "email";
    private final String BIRTHPLACE = "birthplace";
    private final String BIRTHDATE = "birthdate";
    private final String SEX = "sex";
    private final String PHONENUMBER = "phonenumber";
    private final String POINT = "point";


    private PreferenceManager(Context context) {
        mContext = context;
        mEditor = getPref().edit();
    }

    public static PreferenceManager getInstance(Context context) {
        if(mInstance == null)
            mInstance = new PreferenceManager(context);

        return mInstance;
    }

    private SharedPreferences getPref() {
        return mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setFirstTimeUsed(boolean firstTimeUsed) {
        mEditor.putBoolean(FIRST_TIME_USED, firstTimeUsed);
        mEditor.apply();
    }

    public boolean isFirstTimeUsed() {
        return getPref().getBoolean(FIRST_TIME_USED, true);
    }

    public void saveProfile(String id, String token, String fullname, String email,
                            String birthPlace, String birthDate, int sex, String phoneNumber,
                            int point) {
        mEditor.putString(USER_ID, id).
                putString(TOKEN, token).
                putString(FULLNAME, fullname).
                putString(EMAIL, email).
                putString(BIRTHPLACE, birthPlace).
                putString(BIRTHDATE, birthDate).
                putInt(SEX, sex).
                putString(PHONENUMBER, phoneNumber).
                putInt(POINT, point).
                apply();
    }

    public String getId(){
        return getPref().getString(USER_ID, "-1");
    }

    public String getToken(){
        return getPref().getString(TOKEN, "-1");
    }

    public String getFullname(){
        return getPref().getString(FULLNAME, "NO NAME");
    }

    public String getEmail(){
        return getPref().getString(EMAIL, "NO_EMAIL@EMAIL.COM");
    }

    public String getBirthplace(){
        return getPref().getString(BIRTHPLACE, "NO PLACE");
    }

    public String getBirthdate(){
        return getPref().getString(BIRTHDATE, "NO DATE");
    }

    public int getSex(){
        return getPref().getInt(SEX, 0);
    }

    public String getPhonenumber(){
        return getPref().getString(PHONENUMBER, "NO NUMBER");
    }

    public int getPoint(){
        return getPref().getInt(POINT, -1);
    }

    public void setPoint(int point) {
        mEditor.putInt(POINT, point);
        mEditor.apply();
    }

    public void clearProfile() {
        mEditor.remove(USER_ID).
                remove(TOKEN).
                remove(FULLNAME).
                remove(EMAIL).
                remove(BIRTHPLACE).
                remove(BIRTHDATE).
                remove(SEX).
                remove(PHONENUMBER).
                remove(POINT).apply();
    }
}
