package gov.kemenkes.e_jumantik.tool;

/**
 * Created by izzuddiin on 2/20/17.
 */

public class Constant {
    public static String[] indonesianMonth = {"Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "Oktober", "November", "Desember"};

    public static final String RELOAD_ADAPTER = "RELOAD_ADAPTER";

    public static final String MUST_BE_FILLED = "Harus diisi";
    public static final String PASSWORD_NOT_MATCH = "Password dan Konfirmasi Password harus sama";
    public static final String MINIMUM_PASSWORD_NOT_SATISFIED = "Password minimal 4 karakter";
    public static final String INVALID_EMAIL = "Email tidak valid";
    public static final String INVALID_PHONE_NUMBER = "Nomor handphone tidak valid";
    public static final String EMAIL_OR_PASSWORD_INVALID = "Email atau password salah";
    public static final String EMAIL_BEEN_REGISTERED = "Email telah digunakan";
    public static final String EMAIL_INVALID = "Email tidak terdaftar";
    public static final String TOKEN_INVALID = "Token tidak sesuai";
    public static final String USER_ID_INVALID = "ID user tidak valid";

    public static final int GET_BEFORE_OBSERVE_IMAGE = 0;
    public static final int GET_AFTER_OBSERVE_IMAGE = 1;
}
