package gov.kemenkes.e_jumantik.tool;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by izzuddiin on 3/16/17.
 */

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME = "EjumantikDB";

    public static final int VERSION = 1;
}
