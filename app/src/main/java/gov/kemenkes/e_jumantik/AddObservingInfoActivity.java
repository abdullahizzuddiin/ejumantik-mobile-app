package gov.kemenkes.e_jumantik;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.rey.material.app.Dialog;
import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;
import com.rey.material.widget.Spinner;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import gov.kemenkes.e_jumantik.model.District;
import gov.kemenkes.e_jumantik.model.District_Table;
import gov.kemenkes.e_jumantik.model.Province;
import gov.kemenkes.e_jumantik.model.Regency;
import gov.kemenkes.e_jumantik.model.Regency_Table;
import gov.kemenkes.e_jumantik.model.Village;
import gov.kemenkes.e_jumantik.model.Village_Table;
import gov.kemenkes.e_jumantik.presenter.ObservingActivityPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.AddObservingActivityInfoPresenterOperation;
import gov.kemenkes.e_jumantik.tool.Constant;
import gov.kemenkes.e_jumantik.tool.ImageHelper;
import gov.kemenkes.e_jumantik.tool.Utilities;
import gov.kemenkes.e_jumantik.view_operation.AddObservingActivityInfoViewOperaton;

public class AddObservingInfoActivity extends AppCompatActivity implements
        View.OnClickListener,
        AddObservingActivityInfoViewOperaton,
        Spinner.OnItemSelectedListener {
    private TextInputEditText mAddressEt;
    private Spinner mCountrySp;
    private Spinner mProvinceSp;
    private Spinner mRegencySp;
    private Spinner mDistrictSp;
    private Spinner mVillageSp;
    private TextInputEditText mRwEt;
    private TextInputEditText mRtEt;
    private Spinner mPlaceCategorySp;
    private Spinner mPlaceTypeSp;
    private Spinner mContainerTypeSp;
    private TextInputEditText mObservingDateEt;
    private TextInputEditText mNumContainerEt;
    private CheckBox mAnyLarvaeCb;
    private ImageView mBeforeImageIv;
    private ImageView mAfterImageIv;
    private Button mGetImageBeforeBtn;
    private Button mGetImageAfterBtn;
    private Button mSaveBtn;

    private final int GET_BEFORE_OBSERVE_IMAGE = 0;
    private final int GET_AFTER_OBSERVE_IMAGE = 1;

    private Province mDefaultProvince;
    private Regency mDefaultRegency;
    private District mDefaultDistrict;
    private Village mDefaultVillage;

    private List<Province> mProvinceList;
    private List<Regency> mRegencyList;
    private List<District> mDistrictList;
    private List<Village> mVillageList;

    private Uri mUriBefore;
    private Uri mUriAfter;

    private final int INDONESIAN_POSITION = 102;

    private LatLng mSavedLatlng;

    private AddObservingActivityInfoPresenterOperation mPresenter;
    private File mFileBefore;
    private File mFileAfter;
    private Dialog mLoadingDialog;
    private String mLarvaeNestId;
    private String tempDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_observing);
        receiveExtras();
        initiatePresenter();
        initiateView();
        initiateSpinner();
        initiateListener();
        initiateData();
    }

    private void receiveExtras() {
        Bundle extras = getIntent().getExtras();
        mLarvaeNestId = "";
        if(extras != null) {
            mSavedLatlng = (LatLng) extras.get("last_position");
            if(extras.getString("larvae_nest_id") != null)
                mLarvaeNestId = extras.getString("larvae_nest_id");
        }
    }

    private void initiateListener() {
        mGetImageAfterBtn.setOnClickListener(this);
        mGetImageBeforeBtn.setOnClickListener(this);
        mObservingDateEt.setOnClickListener(this);
        mSaveBtn.setOnClickListener(this);

        mCountrySp.setOnItemSelectedListener(this);
        mProvinceSp.setOnItemSelectedListener(this);
        mRegencySp.setOnItemSelectedListener(this);
        mDistrictSp.setOnItemSelectedListener(this);
        mVillageSp.setOnItemSelectedListener(this);

        mCountrySp.setSelection(INDONESIAN_POSITION);
    }

    private void initiatePresenter() {
        mPresenter = new ObservingActivityPresenter(this, this);
    }

    private void initiateView() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAddressEt = (TextInputEditText) findViewById(R.id.et_add_address);
        mCountrySp = (Spinner) findViewById(R.id.sp_add_country);
        mProvinceSp = (Spinner) findViewById(R.id.sp_add_province);
        mRegencySp = (Spinner) findViewById(R.id.sp_add_regency);
        mDistrictSp = (Spinner) findViewById(R.id.sp_add_district);
        mVillageSp = (Spinner) findViewById(R.id.sp_add_village);
        mRwEt = (TextInputEditText) findViewById(R.id.et_add_rw);
        mRtEt = (TextInputEditText) findViewById(R.id.et_add_rt);
        mPlaceCategorySp = (Spinner) findViewById(R.id.sp_add_place_category);
        mPlaceTypeSp = (Spinner) findViewById(R.id.sp_add_place_type);
        mContainerTypeSp = (Spinner) findViewById(R.id.sp_add_container_type);
        mObservingDateEt = (TextInputEditText) findViewById(R.id.et_add_observing_date);
        mNumContainerEt = (TextInputEditText) findViewById(R.id.et_add_container_num);
        mAnyLarvaeCb = (CheckBox) findViewById(R.id.cb_add_any_larvae);
        mGetImageBeforeBtn = (Button) findViewById(R.id.btn_upload_before_observe);
        mGetImageAfterBtn = (Button) findViewById(R.id.btn_upload_after_observe);
        mBeforeImageIv = (ImageView) findViewById(R.id.before_observe_pic_add_iv);
        mAfterImageIv = (ImageView) findViewById(R.id.after_observe_pic_add_iv);
        mSaveBtn = (Button) findViewById(R.id.btn_save);
    }

    private void initiateData() {
        if(!mLarvaeNestId.isEmpty())
            mPresenter.loadData
                    (mLarvaeNestId);
    }

    private void initiateSpinner() {
        initiateDefaultValue();
        Utilities.initiateSpinnerFromJson(mCountrySp, "countries.json");
        Utilities.initiateSpinnerFromList(mProvinceSp, Arrays.asList("Provinsi"));
        Utilities.initiateSpinnerFromList(mRegencySp, Arrays.asList("Kota/Kab"));
        Utilities.initiateSpinnerFromList(mDistrictSp, Arrays.asList("Kecamatan"));
        Utilities.initiateSpinnerFromList(mVillageSp, Arrays.asList("Kelurahan"));
        Utilities.initiateSpinnerFromResource(mPlaceCategorySp, R.array.place_category);
        Utilities.initiateSpinnerFromResource(mPlaceTypeSp, R.array.place_type);
        Utilities.initiateSpinnerFromResource(mContainerTypeSp, R.array.container_type);
    }

    private void initiateDefaultValue() {
        mDefaultProvince = new Province();
        mDefaultProvince.setId("-1");
        mDefaultProvince.setName("Provinsi");

        mDefaultRegency = new Regency();
        mDefaultRegency.setId("-1");
        mDefaultRegency.setName("Kota");

        mDefaultDistrict = new District();
        mDefaultDistrict.setId("-1");
        mDefaultDistrict.setName("Kecamatan");


        mDefaultVillage = new Village();
        mDefaultVillage.setId("-1");
        mDefaultVillage.setName("Kelurahan");
    }

    @Override
    public void showLoadingDialog() {
        mLoadingDialog = new Dialog(this);
        mLoadingDialog.cancelable(false).contentView(R.layout.dialog_loading).show();
    }

    @Override
    public void dismissLoadingDialog() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void setAddress(String address) {
        mAddressEt.setText(address);
    }

    @Override
    public void setRt(String rt) {
        mRtEt.setText(rt);
    }

    @Override
    public void setRw(String rw) {
        mRwEt.setText(rw);
    }

    @Override
    public void setVillage(String village) {
        int selected = 0;
        for(int ii = 0; ii<mVillageList.size(); ii++) {
            if(mVillageList.get(ii).getName().equals(village)) {
                selected = ii;
                ii = mVillageList.size();
            }
        }
        mVillageSp.setSelection(selected);
    }

    @Override
    public void setDistrict(String district) {
        int selected = 0;
        for(int ii = 0; ii<mDistrictList.size(); ii++) {
            if(mDistrictList.get(ii).getName().equals(district)) {
                selected = ii;
                ii = mDistrictList.size();
            }
        }
        mDistrictSp.setSelection(selected);
    }

    @Override
    public void setRegency(String regency) {
        int selected = 0;
        for(int ii = 0; ii<mRegencyList.size(); ii++) {
            if(mRegencyList.get(ii).getName().equals(regency)) {
                selected = ii;
                ii = mRegencyList.size();
            }
        }
        mRegencySp.setSelection(selected);
    }

    @Override
    public void setProvince(String province) {
        int selected = 0;
        for(int ii = 0; ii<mProvinceList.size(); ii++) {
            if(mProvinceList.get(ii).getName().equals(province)) {
                selected = ii;
                ii = mProvinceList.size();
            }
        }
        mProvinceSp.setSelection(selected);
    }

    @Override
    public void setCountry(String country) {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) mCountrySp.getAdapter();
        int spinnerPosition = adapter.getPosition(country);
        mCountrySp.setSelection(spinnerPosition);
    }

    @Override
    public void setPlaceCategory(int placeCategory) {
        mPlaceCategorySp.setSelection(placeCategory);
    }

    @Override
    public void setPlaceType(int placeType) {
        mPlaceTypeSp.setSelection(placeType);
    }

    @Override
    public void setNumContainer(String numContainer) {
        mNumContainerEt.setText(numContainer);
    }

    @Override
    public void setContainerType(int containerType) {
        mContainerTypeSp.setSelection(containerType);
    }

    @Override
    public void setAnyLarvae(boolean anyLarvae) {
        mAnyLarvaeCb.setChecked(anyLarvae);
    }

    @Override
    public void setBeforeImage(String url, File file) {
        if(file == null)
            Picasso.with(this).load(url).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mBeforeImageIv);
        else
            Picasso.with(this).load(file).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mBeforeImageIv);

    }

    @Override
    public void setAfterImage(String url, File file) {
        if(file == null)
            Picasso.with(this).load(url).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mAfterImageIv);
        else {
            Picasso.with(this).load(file).config(Bitmap.Config.RGB_565).
                    fit().centerInside().into(mAfterImageIv);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.homeAsUp) {
            onBackPressed();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }

    @Override
    public void onItemSelected(Spinner parent, View view, int position, long id) {
        int spinnerId = parent.getId();

        switch (spinnerId) {
            case R.id.sp_add_country:
                if(position == INDONESIAN_POSITION) {
                    mProvinceList = SQLite.select().
                            from(Province.class).
                            queryList();
                    mProvinceList.add(0, mDefaultProvince);
                    Utilities.initiateSpinnerFromList(mProvinceSp, mProvinceList);
                } else {
                    Utilities.initiateSpinnerFromList(mProvinceSp, Arrays.asList("Provinsi"));
                }
                break;
            case R.id.sp_add_province:
                Province mProvince = (Province) parent.getSelectedItem();
                if(!mProvince.getId().equals("-1")) {
                    mRegencyList = SQLite.select().
                            from(Regency.class).
                            where(Regency_Table.provinceId.eq(mProvince.getId())).queryList();

                    mRegencyList.add(0, mDefaultRegency);
                    Utilities.initiateSpinnerFromList(mRegencySp, mRegencyList);
                }
                break;
            case R.id.sp_add_regency:
                Regency mRegency= (Regency) parent.getSelectedItem();
                if(!mRegency.getId().equals("-1")) {
                    mDistrictList = SQLite.select().
                            from(District.class).
                            where(District_Table.regencyId.eq(mRegency.getId())).queryList();
                    mDistrictList.add(0, mDefaultDistrict);
                    Utilities.initiateSpinnerFromList(mDistrictSp, mDistrictList);
                }
                break;
            case R.id.sp_add_district:
                District mDistrict = (District) parent.getSelectedItem();
                if(!mDistrict.getId().equals("-1")) {
                    mVillageList = SQLite.select().
                            from(Village.class).
                            where(Village_Table.districtId.eq(mDistrict.getId())).queryList();
                    mVillageList.add(0, mDefaultVillage);
                    Utilities.initiateSpinnerFromList(mVillageSp, mVillageList);
                }
                break;
            case R.id.sp_add_village:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri mUri = null;
        File mFile = null;
        if(resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GET_BEFORE_OBSERVE_IMAGE:
                    mFile = ImageHelper.getImageFromResult(this, resultCode, data);
                    Picasso.with(this).load(mFile).config(Bitmap.Config.RGB_565).fit().centerInside().into(mBeforeImageIv);
//                    mUriBefore = mUri;
                    mFileBefore = mFile;
                    // TODO use bitmap
                    break;
                case GET_AFTER_OBSERVE_IMAGE:
                    mFile = ImageHelper.getImageFromResult(this, resultCode, data);
                    Picasso.with(this).load(mFile).config(Bitmap.Config.RGB_565).fit().centerInside().into(mAfterImageIv);
//                    mUriAfter = mUri;
                    mFileAfter = mFile;
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        else if (resultCode == Activity.RESULT_CANCELED)
            showToast("Pengambilan Foto Batal");
    }

    @Override
    public void navigateToMainView(String instruction) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("instruction", instruction);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void findImage(int requestCode) {
        startActivityForResult(ImageHelper.getPickImageIntent(this), requestCode);
    }

    @Override
    public void showDatePicker() {
        Calendar cal = Calendar.getInstance();

        final android.app.DatePickerDialog date = new android.app.DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                setObservingDate(dayOfMonth, month, year);
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        date.getDatePicker().setMaxDate(new Date().getTime());
        date.show();

//        final DatePickerDialog date = new DatePickerDialog(this);
//        date.applyStyle(R.style.DatePickerStyle);
//        date.dateRange(1, 0, 1920, 31,11,2070);
//        date.date(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
//        date.positiveAction("Simpan");
//        date.positiveActionClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setObservingDate(date.getCalendar());
//                date.dismiss();
//            }
//        });
//
//        date.show();
    }

    private void setObservingDate(int dayOfMonth, int month, int year) {
        String monthString = Constant.indonesianMonth[month];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        tempDate = dateFormat.format(new Date(year-1900, month-1, dayOfMonth));
        mObservingDateEt.setText(getResources().getString(R.string.date_string,dayOfMonth, monthString, year));
    }

    private void setObservingDate(Calendar c) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        tempDate= dateFormat.format(c.getTime());
        mObservingDateEt.setText(Utilities.getPrettyDate(this, c));
    }

    @Override
    public void setObservingDate(String date) {
        tempDate = date;
        mObservingDateEt.setText(Utilities.getPrettyDate(this, Utilities.convertDate(date)));
    }

    @Override
    public String getAddress() {
        return mAddressEt.getText().toString();
    }

    @Override
    public String getRt() {
        return mRtEt.getText().toString();
    }

    @Override
    public String getRw() {
        return mRwEt.getText().toString();
    }

    @Override
    public Province getProvince() {
        if(mProvinceSp.getSelectedItemPosition() == 0)
            return mDefaultProvince;

        return (Province) mProvinceSp.getSelectedItem();
    }

    @Override
    public Regency getRegency() {
        if(mRegencySp.getSelectedItemPosition() == 0)
            return mDefaultRegency;

        return (Regency) mRegencySp.getSelectedItem();
    }

    @Override
    public District getDistrict() {
        if(mDistrictSp.getSelectedItemPosition() == 0)
            return mDefaultDistrict;

        return (District) mDistrictSp.getSelectedItem();
    }

    @Override
    public Village getVillage() {
        if(mVillageSp.getSelectedItemPosition() == 0)
            return mDefaultVillage;

        return (Village) mVillageSp.getSelectedItem();
    }

    @Override
    public String getCountry() {
        return (String) mCountrySp.getSelectedItem();
    }

    @Override
    public int getPlaceType() {
        return mPlaceTypeSp.getSelectedItemPosition();
    }

    @Override
    public int getPlaceCategory() {
        return mPlaceCategorySp.getSelectedItemPosition();
    }

    @Override
    public int getContainerType() {
        return mContainerTypeSp.getSelectedItemPosition();
    }

    @Override
    public int getNumContainer() {
        if(mNumContainerEt.getText().toString().equals(""))
            return -1;

        return Integer.parseInt(mNumContainerEt.getText().toString());
    }

    @Override
    public int getAnyLarvae() {
        return mAnyLarvaeCb.isChecked()?1:0;
    }

    @Override
    public LatLng getSavedPosition() {
        return mSavedLatlng;
    }

    @Override
    public String getBeforeImageFilename() {
        if(mFileBefore == null)
            return "";

        return mFileBefore.getName();
    }

    @Override
    public String getAfterImageFilename() {
        if(mFileAfter == null)
            return "";

        return mFileAfter.getName();
    }

    @Override
    public String getObservingDate() {
        return tempDate;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void setNumContainerError(String message) {
        mNumContainerEt.setError(message);
    }

    @Override
    public void setDateError(String message) {
        mObservingDateEt.setError(message);
    }

    @Override
    public void setRwError(String message) {
        mRwEt.setError(message);
    }

    @Override
    public void setRtError(String message) {
        mRtEt.setError(message);
    }

    @Override
    public void setAddressError(String message) {
        mAddressEt.setError(message);
    }
}
