package gov.kemenkes.e_jumantik;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by izzuddiin on 4/24/17.
 */

public class BootReceiver extends BroadcastReceiver {
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "NERIMA NIH DARI BOOOT");
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d("TAG", "BOOT COMPLETED");
            alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            Intent intentAlarm = new Intent(context, BootReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 0, intentAlarm, 0);

            // Set the alarm to start at 8:30 a.m.
            Calendar calendar = Calendar.getInstance();

            //how many days until sunday
            int days = Calendar.SUNDAY + (7 - calendar.get(Calendar.DAY_OF_WEEK));
            calendar.add(Calendar.DATE, days);
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 9);

            // setRepeating() lets you specify a precise custom interval--in this case,
            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    24 * 7 * 60 * 60 * 1000, alarmIntent);
        } else {
            AlarmReceiver mAlarmReceiver = new AlarmReceiver();
            mAlarmReceiver.createNotification(context);
        }
    }


}
