package gov.kemenkes.e_jumantik;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.rey.material.widget.RadioButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.RegisterPresenterOperation;
import gov.kemenkes.e_jumantik.tool.Constant;
import gov.kemenkes.e_jumantik.tool.Utilities;
import gov.kemenkes.e_jumantik.view_operation.RegisterViewOperation;

public class RegisterActivity extends AppCompatActivity implements
        RegisterViewOperation,
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, TextView.OnEditorActionListener {

    private TextInputEditText mNameEt;
    private TextInputEditText mEmailEt;
    private TextInputEditText mBirthPlaceEt;
    private TextInputEditText mBirthDateEt;
    private RadioGroup mSexRg;
    private RadioButton mSexMaleRb;
    private RadioButton mSexFemaleRb;
    private TextInputEditText mPhoneNumberEt;
    private TextInputEditText mPasswordEt;
    private TextInputEditText mConfPasswordEt;
    private Button mRegisterBtn;

    private RegisterPresenterOperation mPresenter;

    private String mFacebookId;
    private String realDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initiatePresenter();
        initiateView();
        receiveExtras();
        initiateListener();
    }

    private void receiveExtras() {
        Bundle mBundle = getIntent().getExtras();
        if(mBundle != null) {
            if(mBundle.getString("facebookId") != null)
                mFacebookId = mBundle.getString("facebookId");
            else
                mFacebookId = "";
            if (mBundle.getString("first_name") != null)
                mNameEt.setText(mBundle.getString("first_name"));
            if (mBundle.getString("last_name") != null)
                mNameEt.setText(mNameEt.getText().toString()+" "+mBundle.getString("last_name"));
            if (mBundle.getString("email") != null)
                mEmailEt.setText(mBundle.getString("email"));
            if (mBundle.getString("gender") != null) {
                String genderTemp = mBundle.getString("gender");
                if(genderTemp.equals("male"))
                    mSexMaleRb.setChecked(true);
                else
                    mSexFemaleRb.setChecked(true);
            }
        }
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateView() {
        getWindow().setBackgroundDrawable(null);
        mNameEt = (TextInputEditText) findViewById(R.id.et_register_name);
        mEmailEt = (TextInputEditText) findViewById(R.id.et_register_email);
        mBirthPlaceEt = (TextInputEditText) findViewById(R.id.et_register_birth_place);
        mBirthDateEt = (TextInputEditText) findViewById(R.id.et_register_birth_date);
        mSexRg = (RadioGroup) findViewById(R.id.rg_sex);
        mSexMaleRb = (RadioButton) findViewById(R.id.rb_sex_male);
        mPhoneNumberEt = (TextInputEditText) findViewById(R.id.et_register_phone_number);
        mSexFemaleRb = (RadioButton) findViewById(R.id.rb_sex_female);
        mPasswordEt = (TextInputEditText) findViewById(R.id.et_register_password);
        mConfPasswordEt = (TextInputEditText) findViewById(R.id.et_register_conf_password);
        mRegisterBtn = (Button) findViewById(R.id.btn_register);
    }

    private void initiateListener() {
        mRegisterBtn.setOnClickListener(this);
        mBirthDateEt.setOnClickListener(this);
        mConfPasswordEt.setOnEditorActionListener(this);
        mSexMaleRb.setOnCheckedChangeListener(this);
        mSexFemaleRb.setOnCheckedChangeListener(this);
    }

    @Override
    public String getName() {
        return mNameEt.getText().toString();
    }

    @Override
    public String getEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public String getBirthPlace() {
        return mBirthPlaceEt.getText().toString();
    }

    @Override
    public String getBirthDate() {
//        return mBirthDateEt.getText().toString();
        return realDate;
    }

    @Override
    public boolean getSex() {
        if(isMaleRbChecked())
            return true;
        else if(isFemaleRbChecked())
            return false;

        return false;
    }

    @Override
    public String getPhoneNumber() {
        return mPhoneNumberEt.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordEt.getText().toString();
    }

    @Override
    public String getConfPassword() {
        return mConfPasswordEt.getText().toString();
    }

    @Override
    public void setNameError(String message) {
        mNameEt.setError(message);
    }

    @Override
    public void setEmailError(String message) {
        mEmailEt.setError(message);
    }

    @Override
    public void setBirthPlaceError(String message) {
        mBirthPlaceEt.setError(message);
    }

    @Override
    public void setBirthDateError(String message) {
        mBirthDateEt.setError(message);
    }

    @Override
    public void setPhoneNumberError(String message) {
        mPhoneNumberEt.setError(message);
    }

    @Override
    public void setPasswordError(String message) {
        mPasswordEt.setError(message);
    }

    @Override
    public void setConfPasswordError(String message) {
        mConfPasswordEt.setError(message);
    }

    @Override
    public void setSexError(String message) {
        mSexFemaleRb.setError(message);
    }

    @Override
    public boolean isMaleRbChecked() {
        return mSexMaleRb.isChecked();
    }

    @Override
    public boolean isFemaleRbChecked() {
        return mSexFemaleRb.isChecked();
    }

    @Override
    public void showDatePicker() {
        Calendar cal = Calendar.getInstance();

        final DatePickerDialog date = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                setBirthDate(dayOfMonth, month, year);
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        date.updateDate(1970,0,1);
        date.getDatePicker().setMaxDate(new Date().getTime());
        date.show();
    }

    private void setBirthDate(int day, int month, int year) {
        String monthString = Constant.indonesianMonth[month];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        realDate = dateFormat.format(new Date(year-1900, month-1, day));
        mBirthDateEt.setText(getResources().getString(R.string.date_string,day, monthString, year));
    }

    /**
     * tidak dipakai lagi
     * @param c
     */
    private void setBirthDate(Calendar c) {
//        String month = Constant.indonesianMonth[c.get(Calendar.MONTH)];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        realDate = dateFormat.format(c.getTime());
//        mBirthDateEt.setText(getResources().getString(R.string.date,c,month,c));
        mBirthDateEt.setText(Utilities.getPrettyDate(this, c));
    }

    @Override
    public void setBirthDate(String birthDate) {
        mBirthDateEt.setText(birthDate);
    }

    @Override
    public void setMaleRbChecked(boolean isChecked) {
        mSexMaleRb.setChecked(isChecked);
    }

    @Override
    public void setFemaleRbChecked(boolean isChecked) {
        mSexFemaleRb.setChecked(isChecked);
    }

    @Override
    public void navigateToCoverView() {
        onBackPressed();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getFacebookId() {
        return mFacebookId;
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mPresenter.onCheckedChanged(buttonView, isChecked);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return mPresenter.onEditorAction(v, actionId, event);
    }
}
