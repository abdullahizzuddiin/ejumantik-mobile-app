package gov.kemenkes.e_jumantik;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.squareup.picasso.Picasso;

import java.io.File;

import gov.kemenkes.e_jumantik.tool.ImageHelper;
import uk.co.senab.photoview.PhotoView;

public class FullscreenActivity extends AppCompatActivity {
    private String mFilename;
    private String mTitle;
    private PhotoView mFullscreenIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        receiveExtras();
        initiateView();
    }

    private void receiveExtras() {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mFilename = extras.getString("uri");
            mTitle = extras.getString("title");
        }
    }

    private void initiateView() {
        getWindow().setBackgroundDrawable(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(mTitle);
        mFullscreenIv = (PhotoView) findViewById(R.id.iv_fullscreen_pic);
        File mFile = ImageHelper.getImageLocal(mFilename);
        setupImage(ImageHelper.getImageUrl(mFilename), mFile);
    }

    private void setupImage(String imageUrl, File file) {
        if(file != null)
            Picasso.with(this).load(file).config(Bitmap.Config.RGB_565).fit().centerInside().into(mFullscreenIv);
        else
            Picasso.with(this).load(imageUrl).config(Bitmap.Config.RGB_565).fit().centerInside().into(mFullscreenIv);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.homeAsUp) {
            onBackPressed();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        mFullscreenIv.setImageDrawable(null);
        super.onDestroy();
    }
}
