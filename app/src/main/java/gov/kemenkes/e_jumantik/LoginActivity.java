package gov.kemenkes.e_jumantik;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;

import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.LoginPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.LoginViewOperation;

import static gov.kemenkes.e_jumantik.R.id.et_login_password;

public class LoginActivity extends AppCompatActivity implements
        TextView.OnEditorActionListener,
        CompoundButton.OnCheckedChangeListener,
        LoginViewOperation,
        View.OnClickListener {

    private ImageView mLogoIv;
    private TextInputEditText mEmailEt;
    private TextInputEditText mPasswordEt;
    private Button mLoginBtn;
    private CheckBox mShowPasswordCb;
    private TextView mForgotPasswordTv;

    private LoginPresenterOperation mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initiatePresenter();
        initiateView();
        initiateListener();
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateView() {
        getWindow().setBackgroundDrawable(null);
        mLoginBtn = (Button) findViewById(R.id.btn_login);
        mEmailEt = (TextInputEditText) findViewById(R.id.et_login_email);
        mPasswordEt = (TextInputEditText) findViewById(et_login_password);
        mShowPasswordCb = (CheckBox) findViewById(R.id.cb_show_password);
        mForgotPasswordTv = (TextView) findViewById(R.id.tv_login_forgot_password);
        mLogoIv = (ImageView) findViewById(R.id.iv_logo_circle);
    }

    private void initiateListener() {
        mLoginBtn.setOnClickListener(this);
        mPasswordEt.setOnEditorActionListener(this);
        mShowPasswordCb.setOnCheckedChangeListener(this);
        mForgotPasswordTv.setOnClickListener(this);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return mPresenter.onEditorAction(v, actionId, event);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mPresenter.onCheckedChanged(buttonView, isChecked);
    }

    @Override
    public void onClick(View v) {
        mPresenter.onClick(v);
    }

    @Override
    public String getEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordEt.getText().toString();
    }

    @Override
    public void setEmailError(String message) {
        mEmailEt.setError(message);
    }

    @Override
    public void setPasswordError(String message) {
        mPasswordEt.setError(message);
    }

    @Override
    public void showPassword(boolean show) {
        if(!show) {
            mPasswordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mShowPasswordCb.setText(getResources().getString(R.string.cb_show_password));
        }
        else {
            mPasswordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mShowPasswordCb.setText(getResources().getString(R.string.cb_unshow_password));
        }
    }

    @Override
    public void showWrongAccountDialog() {

    }

    @Override
    public void showForgotPasswordDialog() {

    }

    @Override
    public void navigateToMainView() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void navigateToResetPasswordView() {
        Intent i = new Intent(this, ResetPasswordActivity.class);
        startActivity(i);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
