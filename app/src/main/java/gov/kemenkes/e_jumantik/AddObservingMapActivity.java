package gov.kemenkes.e_jumantik;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rey.material.widget.Button;

import gov.kemenkes.e_jumantik.presenter.MapPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.MapPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.MapViewOperation;

public class AddObservingMapActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        PlaceSelectionListener,
        MapViewOperation,
        View.OnClickListener {

    private GoogleMap mMap;
    private UiSettings mUiSettings;
    private PlaceAutocompleteFragment mPlaceAutocompleteFragment;
    private float mMinZoomLevel = 15;
    private float mMaxZoomLevel = 25;
    private int mZoomLevel = 18;
    private SupportMapFragment mapFragment;

    private Button mNextBtn;

    private MapPresenterOperation mMapPresenter;
    private Bundle bundle;
    private LatLng mLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_observing_map);
        receiveExtras();
        initiateView();
        initiateListener();
        initiatePresenter();
    }

    private void receiveExtras() {
        bundle = getIntent().getExtras();
        mLatLng = null;
        if(bundle != null) {
            mLatLng = (LatLng) bundle.get("latlng");
        }
    }

    private void initiateView() {
        setTitle("Pilih Lokasi Pemantauan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mNextBtn = (Button) findViewById(R.id.btn_next_map);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mPlaceAutocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("ID")
                .build();

        mapFragment.getMapAsync(this);
        mPlaceAutocompleteFragment.setFilter(typeFilter);
    }

    private void initiateListener() {
        mPlaceAutocompleteFragment.setOnPlaceSelectedListener(this);
        mNextBtn.setOnClickListener(this);
    }

    private void initiatePresenter() {
        mMapPresenter = new MapPresenter(this, this);
    }

    private void configureMap(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMaxZoomPreference(mMaxZoomLevel);
        mMap.setMinZoomPreference(mMinZoomLevel);
        mMap.setPadding(0,150,0,150);
        mUiSettings = mMap.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setMapToolbarEnabled(false);

        moveCamera(new LatLng(-6.1864864, 106.83409110000002));
    }

    private void checkPermission() {
        //mengecek permission
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        configureMap(googleMap);
        checkPermission();
        mMap.setOnMapClickListener(this);
        if(mLatLng != null)
            mMapPresenter.onMapClick(mLatLng);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMapPresenter.onMapClick(latLng);
    }

    @Override
    public void onPlaceSelected(Place place) {
        placeMarker(place.getLatLng());
        moveCamera(place.getLatLng());
    }

    @Override
    public void onError(Status status) {
        showToast(status.getStatusMessage());
    }

    @Override
    public void onClick(View v) {
        mMapPresenter.onClick(v);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.homeAsUp) {
            onBackPressed();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void navigateToObservingInfoView(LatLng latLng) {
        Intent intent = new Intent(AddObservingMapActivity.this, AddObservingInfoActivity.class);
        intent.putExtra("last_position",latLng);

        if(bundle != null)
            intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    public void moveCamera(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,mZoomLevel));
    }

    @Override
    public void placeMarker(LatLng latLng) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Tempat Pemantauan");
        mMap.addMarker(markerOptions).showInfoWindow();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearMap() {
        mMap.clear();
    }
}
