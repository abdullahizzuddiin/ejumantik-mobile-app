package gov.kemenkes.e_jumantik.presenter_operation;

import android.os.Bundle;
import android.view.View;

/**
 * Created by izzuddiin on 4/1/17.
 */

public interface DetailObservingInfoPresenterOperation {
    void receivedExtras(Bundle bundle);

    void loadData();

    void onClick(View v);
}
