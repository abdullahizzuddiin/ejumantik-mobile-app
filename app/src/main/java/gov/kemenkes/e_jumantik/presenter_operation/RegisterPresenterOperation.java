package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * Created by izzuddiin on 2/19/17.
 */

public interface RegisterPresenterOperation  {
    void onClick(View v);

    void onCheckedChanged(CompoundButton buttonView, boolean isChecked);

    boolean onEditorAction(TextView v, int actionId, KeyEvent event);
}
