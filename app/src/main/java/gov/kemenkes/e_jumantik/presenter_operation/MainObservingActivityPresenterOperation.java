package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.View;

/**
 * Created by izzuddiin on 4/6/17.
 */

public interface MainObservingActivityPresenterOperation {
    void onClick(View v);

    void loadData();
}
