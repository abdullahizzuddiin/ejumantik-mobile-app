package gov.kemenkes.e_jumantik.presenter_operation;

import android.content.Intent;
import android.view.View;

/**
 * Created by izzuddiin on 2/25/17.
 */

public interface CoverPresenterOperation {
    void onClick(View v);

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
