package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * Created by izzuddiin on 2/24/17.
 */

public interface LoginPresenterOperation {
    boolean onEditorAction(TextView v, int actionId, KeyEvent event);

    void onCheckedChanged(CompoundButton buttonView, boolean isChecked);

    void onClick(View v);
}
