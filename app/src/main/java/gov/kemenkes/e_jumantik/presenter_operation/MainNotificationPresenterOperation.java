package gov.kemenkes.e_jumantik.presenter_operation;

/**
 * Created by izzuddiin on 4/7/17.
 */

public interface MainNotificationPresenterOperation {
    void loadNotification();
}
