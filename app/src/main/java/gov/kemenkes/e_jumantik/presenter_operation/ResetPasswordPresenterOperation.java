package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.View;

/**
 * Created by izzuddiin on 4/24/17.
 */

public interface ResetPasswordPresenterOperation {
    void onClick(View v);
}
