package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.View;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by izzuddiin on 3/28/17.
 */

public interface MapPresenterOperation {
    void onMapClick(LatLng latLng);

    void onClick(View view);
}
