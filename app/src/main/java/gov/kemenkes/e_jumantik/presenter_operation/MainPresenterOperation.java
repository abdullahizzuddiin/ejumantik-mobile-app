package gov.kemenkes.e_jumantik.presenter_operation;

/**
 * Created by izzuddiin on 4/6/17.
 */

public interface MainPresenterOperation {
    void logout();
}
