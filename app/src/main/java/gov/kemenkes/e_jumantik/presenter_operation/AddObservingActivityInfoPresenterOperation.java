package gov.kemenkes.e_jumantik.presenter_operation;

import android.view.View;

/**
 * Created by izzuddiin on 3/28/17.
 */

public interface AddObservingActivityInfoPresenterOperation {
    void onClick(View v);

    void loadData(String larvaeNestId);
}
