package gov.kemenkes.e_jumantik.presenter_operation;

/**
 * Created by izzuddiin on 3/31/17.
 */

public interface SplashscreenPresenterOperation {
    void setupData();
}
