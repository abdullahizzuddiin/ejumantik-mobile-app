package gov.kemenkes.e_jumantik;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import gov.kemenkes.e_jumantik.fragment.DrawerFragment;
import gov.kemenkes.e_jumantik.fragment.MainObservingActivityFragment;
import gov.kemenkes.e_jumantik.fragment.NotificationFragment;
import gov.kemenkes.e_jumantik.fragment.PointFragment;
import gov.kemenkes.e_jumantik.presenter.GeneralPresenter;
import gov.kemenkes.e_jumantik.presenter_operation.MainPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.MainViewOperation;

public class MainActivity extends AppCompatActivity implements MainViewOperation{
    private DrawerFragment mDrawerFragment;
    private DrawerLayout mDrawerLayout;
    private MainPresenterOperation mPresenter;

    private Bundle mExtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        receiveExtras();
        initiatePresenter();
        initiateToolbar();
        initiateNavigationDrawer();
        navigateToMosquitoView();
    }

    private void receiveExtras() {
        mExtras = getIntent().getExtras();
    }

    private void initiatePresenter() {
        mPresenter = new GeneralPresenter(this, this);
    }

    private void initiateToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerFragment.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }


    private void initiateNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.initiateViewOperation(this);
        mDrawerFragment.initiateToggle(mDrawerLayout);
    }

    @Override
    public void navigateToMosquitoView() {
        Fragment fragment = new MainObservingActivityFragment();
        if(mExtras != null)
            fragment.setArguments(mExtras);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
        setTitle(getResources().getString(R.string.nav_item_activity));
    }

    @Override
    public void navigateToNotificationView() {
        Fragment fragment = new NotificationFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
        setTitle(getResources().getString(R.string.nav_item_notification));
    }

    @Override
    public void navigateToPointView() {
        Fragment fragment = new PointFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
        setTitle(getResources().getString(R.string.nav_item_point));

    }

    @Override
    public void navigateToCoverView() {
        Intent i = new Intent(this, CoverActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void goLogout() {
        mPresenter.logout();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
