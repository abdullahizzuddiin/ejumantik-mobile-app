package gov.kemenkes.e_jumantik.listener;

import android.view.View;

/**
 * Created by izzuddiin on 3/5/17.
 */

public interface ViewHolderClickListener {
    void onClickRecyclerView(View v, int position, boolean isLongClick);
}
