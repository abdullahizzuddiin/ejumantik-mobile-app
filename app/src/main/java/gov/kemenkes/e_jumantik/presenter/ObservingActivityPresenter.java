package gov.kemenkes.e_jumantik.presenter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.model.Error;
import gov.kemenkes.e_jumantik.model.GeneralInfo;
import gov.kemenkes.e_jumantik.model.LarvaeNest;
import gov.kemenkes.e_jumantik.model.LarvaeNest_Table;
import gov.kemenkes.e_jumantik.presenter_operation.AddObservingActivityInfoPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.DetailObservingInfoPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.MainObservingActivityPresenterOperation;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import gov.kemenkes.e_jumantik.rest_api.ProgressRequestBody;
import gov.kemenkes.e_jumantik.rest_api.services.LarvaeNestService;
import gov.kemenkes.e_jumantik.tool.Constant;
import gov.kemenkes.e_jumantik.tool.ImageHelper;
import gov.kemenkes.e_jumantik.tool.PreferenceManager;
import gov.kemenkes.e_jumantik.tool.Utilities;
import gov.kemenkes.e_jumantik.view_operation.AddObservingActivityInfoViewOperaton;
import gov.kemenkes.e_jumantik.view_operation.DetailObservingInfoViewOperation;
import gov.kemenkes.e_jumantik.view_operation.MainObservingViewOperation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gov.kemenkes.e_jumantik.tool.Constant.GET_AFTER_OBSERVE_IMAGE;
import static gov.kemenkes.e_jumantik.tool.Constant.GET_BEFORE_OBSERVE_IMAGE;

/**
 * Created by izzuddiin on 3/28/17.
 */

public class ObservingActivityPresenter implements
        AddObservingActivityInfoPresenterOperation,
        DetailObservingInfoPresenterOperation,
        MainObservingActivityPresenterOperation, ProgressRequestBody.UploadCallbacks{

    private Context mContext;
    private AddObservingActivityInfoViewOperaton mAddView;
    private DetailObservingInfoViewOperation mDetailView;
    private MainObservingViewOperation mMainView;

    private String mLarvaeNestId;
    private LarvaeNest mLarvaeNest;

    private LarvaeNestService mLarvaeNestService;

    private String tempId;

    public ObservingActivityPresenter(Context context, AddObservingActivityInfoViewOperaton addView) {
        mContext = context;
        mAddView = addView;
        tempId = null;
        mLarvaeNestService = ApiUtils.getLarvaeNestService();
    }

    public ObservingActivityPresenter(Context context, DetailObservingInfoViewOperation detailView) {
        mContext = context;
        mDetailView = detailView;
    }

    public ObservingActivityPresenter(Context context, MainObservingViewOperation mainView) {
        mContext = context;
        mMainView = mainView;
        mLarvaeNestService = ApiUtils.getLarvaeNestService();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.after_observe_pic_detail_iv:
                mDetailView.navigateToFullscreenView(mContext.getResources().getString(R.string.photo_cleaning_activity), mLarvaeNest.getAfterImage());
                break;
            case R.id.before_observe_pic_detail_iv:
                mDetailView.navigateToFullscreenView(mContext.getResources().getString(R.string.photo_observing_activity), mLarvaeNest.getBeforeImage());
                break;
            case R.id.et_add_observing_date:
                mAddView.showDatePicker();
                break;
            case R.id.btn_upload_before_observe:
                mAddView.findImage(GET_BEFORE_OBSERVE_IMAGE);
                break;
            case R.id.btn_upload_after_observe:
                mAddView.findImage(GET_AFTER_OBSERVE_IMAGE);
                break;
            case R.id.fab_update_larvae:
                mDetailView.navigateToMapView(mLarvaeNest.getId(), new LatLng(mLarvaeNest.getLatitude(), mLarvaeNest.getLongitude()));
                break;
            case R.id.btn_save:
                if(validateInfo()) {
                    mAddView.showLoadingDialog();
                    saveObservingInfo();
                } else {
                    mAddView.showToast("Form belum lengkap");
                }
                break;
            case R.id.fab_plus_mosquito_activity:
                mMainView.navigateToAddObservingActivity();
//                uploadTestPicture();
                break;
        }
    }

    @Override
    public void loadData(String larvaeNestId) {
        tempId = larvaeNestId;
        mLarvaeNest = SQLite.select().
                from(LarvaeNest.class).
                where(LarvaeNest_Table.id.eq(larvaeNestId)).querySingle();

        mAddView.setAddress(mLarvaeNest.getAddress());
        mAddView.setRt(mLarvaeNest.getRt());
        mAddView.setRw(mLarvaeNest.getRw());
        mAddView.setCountry(mLarvaeNest.getCountryId());
        mAddView.setProvince(mLarvaeNest.getProvinceId());
        mAddView.setRegency(mLarvaeNest.getRegencyId());
        mAddView.setDistrict(mLarvaeNest.getDistrictId());
        mAddView.setVillage(mLarvaeNest.getVillageId());
        mAddView.setPlaceCategory(mLarvaeNest.getPlaceCategory());
        mAddView.setPlaceType(mLarvaeNest.getPlaceType());
        mAddView.setContainerType(mLarvaeNest.getContainerType());
        mAddView.setNumContainer(String.valueOf(mLarvaeNest.getNumContainer()));
        mAddView.setAnyLarvae(mLarvaeNest.getAnyLarvae()==1?true:false);
        mAddView.setObservingDate(mLarvaeNest.getObservingDate());

        File beforeImageFile = ImageHelper.getImageLocal(mLarvaeNest.getBeforeImage());
        mAddView.setBeforeImage(ImageHelper.getImageUrl(mLarvaeNest.getBeforeImage()), beforeImageFile);

        File afterImageFile = ImageHelper.getImageLocal(mLarvaeNest.getAfterImage());
        mAddView.setAfterImage(ImageHelper.getImageUrl(mLarvaeNest.getAfterImage()), afterImageFile);
    }

    @Override
    public void receivedExtras(Bundle bundle) {
        if(bundle != null) {
            mLarvaeNestId = bundle.getString("larvae_nest_id");
        }
    }

    @Override
    public void loadData() {
        if(mDetailView != null) {
            mLarvaeNest = SQLite.select().
                    from(LarvaeNest.class).
                    where(LarvaeNest_Table.id.eq(mLarvaeNestId)).querySingle();

            mDetailView.setAddress(mLarvaeNest.getAddress());
            mDetailView.setRt(mLarvaeNest.getRt());
            mDetailView.setRw(mLarvaeNest.getRw());
            mDetailView.setProvince(mLarvaeNest.getProvinceId());
            mDetailView.setRegency(mLarvaeNest.getRegencyId());
            mDetailView.setDistrict(mLarvaeNest.getDistrictId());
            mDetailView.setVillage(mLarvaeNest.getVillageId());
            mDetailView.setPlaceCategory(mContext.getResources().
                    getStringArray(R.array.place_category)[mLarvaeNest.getPlaceCategory()]);
            mDetailView.setPlaceType(mContext.getResources().
                    getStringArray(R.array.place_type)[mLarvaeNest.getPlaceType()]);
            mDetailView.setContainerType(mContext.getResources().
                    getStringArray(R.array.container_type)[mLarvaeNest.getContainerType()]);
            mDetailView.setNumContainer(String.valueOf(mLarvaeNest.getNumContainer()));
            mDetailView.setAnyLarvae(mLarvaeNest.getAnyLarvae()==1?"Ada":"Tidak Ada");
            mDetailView.setDate(Utilities.getPrettyDate(mContext, Utilities.convertDate(mLarvaeNest.getObservingDate())));

            File beforeImageFile = ImageHelper.getImageLocal(mLarvaeNest.getBeforeImage());
            mDetailView.setBeforeImage(ImageHelper.getImageUrl(mLarvaeNest.getBeforeImage()), beforeImageFile);

            File afterImageFile = ImageHelper.getImageLocal(mLarvaeNest.getAfterImage());
            mDetailView.setAfterImage(ImageHelper.getImageUrl(mLarvaeNest.getAfterImage()), afterImageFile);

        } else if (mMainView != null) {
            String userId = PreferenceManager.getInstance(mContext).getId();
            String token = PreferenceManager.getInstance(mContext).getToken();
            mLarvaeNestService.getAllLarvae(userId, token).enqueue(new Callback<List<LarvaeNest>>() {
                @Override
                public void onResponse(Call<List<LarvaeNest>> call, Response<List<LarvaeNest>> response) {
                    if(response.isSuccessful()) {
                        mMainView.setSwipeRefreshing(false);
                        mMainView.updateAdapter(response.body());
                        LarvaeNest.saveListLarvaeNest(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<LarvaeNest>> call, Throwable t) {
                    Log.d("TAG", t.getMessage());
                    mMainView.setSwipeRefreshing(false);
                    mMainView.updateAdapter(getLarvaeNestFromDb());
                }
            });
        }
    }

    private List<LarvaeNest> getLarvaeNestFromDb() {
        return SQLite.select()
                .from(LarvaeNest.class)
                .where(LarvaeNest_Table.userId.eq(PreferenceManager.getInstance(mContext).getId()))
                .queryList();
    }

    private void saveObservingInfo() {
        LarvaeNest item = new LarvaeNest();
        item.setUserId(PreferenceManager.getInstance(mContext).getId());
        item.setAddress(mAddView.getAddress());
        item.setCountryId(mAddView.getCountry());
        item.setProvinceId(mAddView.getProvince().getName());
        item.setRegencyId(mAddView.getRegency().getName());
        item.setDistrictId(mAddView.getDistrict().getName());
        item.setVillageId(mAddView.getVillage().getName());
        item.setRw(mAddView.getRw());
        item.setRt(mAddView.getRt());
        item.setAnyLarvae(mAddView.getAnyLarvae());
        item.setObservingDate(mAddView.getObservingDate());
        item.setNumContainer(mAddView.getNumContainer());
        item.setLatitude((float) (mAddView.getSavedPosition().latitude));
        item.setLongitude((float) (mAddView.getSavedPosition().longitude));
        item.setPlaceCategory(mAddView.getPlaceCategory());
        item.setPlaceType(mAddView.getPlaceType());
        item.setContainerType(mAddView.getContainerType());
        item.setBeforeImage(mAddView.getBeforeImageFilename());
        item.setAfterImage(mAddView.getAfterImageFilename());

        if(tempId != null) {
            item.setId(tempId);
            uploadObservingInfoUpdate(item);
        } else {
            uploadObservingInfoNew(item);
        }
    }

    private void uploadObservingInfoUpdate(final LarvaeNest larvaeNest) {
        File mFileBefore = ImageHelper.getImageLocal(larvaeNest.getBeforeImage());
        File mFileAfter = ImageHelper.getImageLocal(larvaeNest.getAfterImage());

        RequestBody requestFileBefore;
        MultipartBody.Part mBodyBefore = null;

        RequestBody requestFileAfter;
        MultipartBody.Part mBodyAfter = null;


        if(mFileBefore != null) {
            requestFileBefore = RequestBody.create(MediaType.parse("image/*"), mFileBefore);
            mBodyBefore = MultipartBody.Part.createFormData("foto_before", mFileBefore.toString(), requestFileBefore);
        }

        if(mFileAfter != null) {
            requestFileAfter = RequestBody.create(MediaType.parse("image/*"), mFileAfter);
            mBodyAfter = MultipartBody.Part.createFormData("foto_after", mFileAfter.toString(), requestFileAfter);
        }

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("id", createPart(larvaeNest.getId()));
        map.put("user_id", createPart(PreferenceManager.getInstance(mContext).getId()));
        map.put("token", createPart(PreferenceManager.getInstance(mContext).getToken()));
        map.put("country", createPart(larvaeNest.getCountryId()));
        map.put("province", createPart(larvaeNest.getProvinceId()));
        map.put("address", createPart(larvaeNest.getAddress()));
        map.put("city", createPart(larvaeNest.getRegencyId()));
        map.put("district", createPart(larvaeNest.getDistrictId()));
        map.put("village", createPart(larvaeNest.getVillageId()));
        map.put("rw", createPart(larvaeNest.getRw()));
        map.put("rt", createPart(larvaeNest.getRt()));
        map.put("any_larvae",createPart(String.valueOf(larvaeNest.getAnyLarvae())));
        map.put("observing_date", createPart(larvaeNest.getObservingDate()));
        map.put("container_category", createPart(String.valueOf(larvaeNest.getContainerType())));
        map.put("place_category", createPart(String.valueOf(larvaeNest.getPlaceCategory())));
        map.put("place_type", createPart(String.valueOf(larvaeNest.getPlaceType())));
        map.put("longitude", createPart(String.valueOf(larvaeNest.getLongitude())));
        map.put("latitude", createPart(String.valueOf(larvaeNest.getLatitude())));
        map.put("num_container", createPart(String.valueOf(larvaeNest.getNumContainer())));

        if(mBodyBefore == null && mBodyAfter == null) {
            mLarvaeNestService.updateLarvaeEmpty(map).enqueue(new Callback<GeneralInfo>() {
                @Override
                public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                    if(response.isSuccessful()) {
                        FlowManager.getModelAdapter(LarvaeNest.class).save(larvaeNest);
                        mAddView.navigateToMainView(Constant.RELOAD_ADAPTER);

                    } else {
                        Error mError = ApiUtils.parseError(response);
                        mAddView.showToast(mError.getMessage());
                    }
                    mAddView.dismissLoadingDialog();
                }

                @Override
                public void onFailure(Call<GeneralInfo> call, Throwable t) {
                    mAddView.showToast(t.getMessage());
                    mAddView.dismissLoadingDialog();
                }
            });
        }else if(mBodyBefore == null) {
            mLarvaeNestService.updateLarvaeParcial(map, mBodyAfter).enqueue(new Callback<GeneralInfo>() {
                @Override
                public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                    if(response.isSuccessful()) {
                        FlowManager.getModelAdapter(LarvaeNest.class).save(larvaeNest);
                        mAddView.navigateToMainView(Constant.RELOAD_ADAPTER);

                    } else {
                        Error mError = ApiUtils.parseError(response);
                        mAddView.showToast(mError.getMessage());
                    }
                    mAddView.dismissLoadingDialog();
                }

                @Override
                public void onFailure(Call<GeneralInfo> call, Throwable t) {
                    mAddView.showToast(t.getMessage());
                    mAddView.dismissLoadingDialog();
                }
            });
        } else if (mBodyAfter == null) {
            mLarvaeNestService.updateLarvaeParcial(map, mBodyBefore).enqueue(new Callback<GeneralInfo>() {
                @Override
                public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                    if(response.isSuccessful()) {
                        FlowManager.getModelAdapter(LarvaeNest.class).save(larvaeNest);
                        mAddView.navigateToMainView(Constant.RELOAD_ADAPTER);

                    } else {
                        Error mError = ApiUtils.parseError(response);
                        mAddView.showToast(mError.getMessage());
                    }
                    mAddView.dismissLoadingDialog();
                }

                @Override
                public void onFailure(Call<GeneralInfo> call, Throwable t) {
                    mAddView.showToast(t.getMessage());
                    mAddView.dismissLoadingDialog();
                }
            });
        } else  {
            mLarvaeNestService.updateLarvae(map, mBodyBefore, mBodyAfter).enqueue(new Callback<GeneralInfo>() {
                @Override
                public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                    if(response.isSuccessful()) {
                        FlowManager.getModelAdapter(LarvaeNest.class).save(larvaeNest);
                        mAddView.navigateToMainView(Constant.RELOAD_ADAPTER);

                    } else {
                        Error mError = ApiUtils.parseError(response);
                        mAddView.showToast(mError.getMessage());
                    }
                    mAddView.dismissLoadingDialog();
                }

                @Override
                public void onFailure(Call<GeneralInfo> call, Throwable t) {
                    mAddView.showToast(t.getMessage());
                    mAddView.dismissLoadingDialog();
                }
            });
        }
    }



    private void uploadObservingInfoNew(final LarvaeNest larvaeNest) {
        File mFileBefore = ImageHelper.getImageLocal(larvaeNest.getBeforeImage());
        File mFileAfter = ImageHelper.getImageLocal(larvaeNest.getAfterImage());

        RequestBody requestFileBefore = RequestBody.create(MediaType.parse("image/*"), mFileBefore);
        RequestBody requestFileAfter = RequestBody.create(MediaType.parse("image/*"), mFileAfter);

        MultipartBody.Part mBodyBefore = MultipartBody.Part.createFormData("foto_before", mFileBefore.toString(), requestFileBefore);
        MultipartBody.Part mBodyAfter = MultipartBody.Part.createFormData("foto_after", mFileAfter.toString(), requestFileAfter);

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", createPart(PreferenceManager.getInstance(mContext).getId()));
        map.put("token", createPart(PreferenceManager.getInstance(mContext).getToken()));
        map.put("country", createPart(larvaeNest.getCountryId()));
        map.put("province", createPart(larvaeNest.getProvinceId()));
        map.put("address", createPart(larvaeNest.getAddress()));
        map.put("city", createPart(larvaeNest.getRegencyId()));
        map.put("district", createPart(larvaeNest.getDistrictId()));
        map.put("village", createPart(larvaeNest.getVillageId()));
        map.put("rw", createPart(larvaeNest.getRw()));
        map.put("rt", createPart(larvaeNest.getRt()));
        map.put("any_larvae",createPart(String.valueOf(larvaeNest.getAnyLarvae())));
        map.put("observing_date", createPart(larvaeNest.getObservingDate()));
        map.put("container_category", createPart(String.valueOf(larvaeNest.getContainerType())));
        map.put("place_category", createPart(String.valueOf(larvaeNest.getPlaceCategory())));
        map.put("place_type", createPart(String.valueOf(larvaeNest.getPlaceType())));
        map.put("longitude", createPart(String.valueOf(larvaeNest.getLongitude())));
        map.put("latitude", createPart(String.valueOf(larvaeNest.getLatitude())));
        map.put("num_container", createPart(String.valueOf(larvaeNest.getNumContainer())));

        mLarvaeNestService.createLarvae(map, mBodyBefore, mBodyAfter).enqueue(new Callback<GeneralInfo>() {
            @Override
            public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                if(response.isSuccessful()) {
                    larvaeNest.setId(response.body().getData());
                    FlowManager.getModelAdapter(LarvaeNest.class).save(larvaeNest);
                    PreferenceManager.getInstance(mContext).setPoint(PreferenceManager.getInstance(mContext).getPoint()+10);
                    mAddView.navigateToMainView(Constant.RELOAD_ADAPTER);

                } else {
                    Error mError = ApiUtils.parseError(response);
                    mAddView.showToast(mError.getMessage());
                }
                mAddView.dismissLoadingDialog();
            }

            @Override
            public void onFailure(Call<GeneralInfo> call, Throwable t) {
                mAddView.showToast(t.getMessage());
                mAddView.dismissLoadingDialog();
            }
        });
    }

    private RequestBody createPart(String content) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, content);
    }

    private void uploadTestPicture() {
        String filenameBefore = "IMG_20170405_002904.jpg";
        String filenameAfter = "IMG_20170331_081736.jpg";
        File mFileBefore = ImageHelper.getImageLocal(filenameBefore);
        File mFileAfter = ImageHelper.getImageLocal(filenameAfter);
        Uri mUriBefore = Uri.parse(mFileBefore.toString());
        Uri mUriAfter = Uri.parse(mFileAfter.toString());

//        RequestBody requestFileBefore = RequestBody.create(MediaType.parse("image/*"), mFileBefore);

        ProgressRequestBody requestFileBefore = new ProgressRequestBody(mFileBefore, this, true);

//        RequestBody requestFileAfter = RequestBody.create(MediaType.parse("image/*"), mFileAfter);

        ProgressRequestBody requestFileAfter = new ProgressRequestBody(mFileAfter, this, false);

        MultipartBody.Part mBodyBefore = MultipartBody.Part.createFormData("foto_before", mFileBefore.toString(), requestFileBefore);

        MultipartBody.Part mBodyAfter = MultipartBody.Part.createFormData("foto_after", mFileAfter.toString(), requestFileAfter);

        mLarvaeNestService.uploadTestPictre(mBodyBefore, mBodyAfter).enqueue(new Callback<GeneralInfo>() {
            @Override
            public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                Log.d("TAG", response.body().toString());
            }

            @Override
            public void onFailure(Call<GeneralInfo> call, Throwable t) {
                Log.d("TAG", t.getMessage());
            }
        });
    }

    private boolean validateInfo() {
        boolean isTrue = true;
        if(!Utilities.isFormFilled(mAddView.getAddress())) {
            mAddView.setAddressError(Constant.MUST_BE_FILLED);
            isTrue = false;
        }
        if (!Utilities.isFormFilled(mAddView.getRt())) {
            mAddView.setRtError(Constant.MUST_BE_FILLED);
            isTrue = false;
        }
        if (!Utilities.isFormFilled(mAddView.getRw())) {
            mAddView.setRwError(Constant.MUST_BE_FILLED);
            isTrue = false;
        }
        if (mAddView.getVillage().getId().equals("-1"))
            isTrue = false;
        if (mAddView.getDistrict().getId().equals("-1"))
            isTrue = false;
        if (mAddView.getRegency().getId().equals("-1"))
            isTrue = false;
        if (mAddView.getProvince().getId().equals("-1"))
            isTrue = false;
        if (mAddView.getPlaceCategory() == 0)
            isTrue = false;
        if (mAddView.getPlaceType() == 0)
            isTrue = false;
        if (mAddView.getContainerType() == 0)
            isTrue = false;
        if (!Utilities.isFormFilled(mAddView.getObservingDate())) {
            mAddView.setDateError(Constant.MUST_BE_FILLED);
            isTrue = false;
        }
        if (mAddView.getNumContainer() == -1) {
            mAddView.setNumContainerError(Constant.MUST_BE_FILLED);
            isTrue = false;
        }

        return isTrue;
    }

    @Override
    public void onProgressUpdate(int percentage, boolean isBeforeImage) {
        Log.d("TAG","PROGRESS = "+ (isBeforeImage?"BEFORE IMAGE":"AFTER IMAGE") + percentage);
    }

    @Override
    public void onError() {
        Log.d("TAG","PROGRESS ERROR");
    }

    @Override
    public void onFinish() {
        Log.d("TAG","PROGRESS = kelar");

    }
}
