package gov.kemenkes.e_jumantik.presenter;

import android.content.Context;

import java.util.List;

import gov.kemenkes.e_jumantik.model.Error;
import gov.kemenkes.e_jumantik.model.Notification;
import gov.kemenkes.e_jumantik.presenter_operation.MainNotificationPresenterOperation;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import gov.kemenkes.e_jumantik.rest_api.services.NotificationService;
import gov.kemenkes.e_jumantik.tool.PreferenceManager;
import gov.kemenkes.e_jumantik.view_operation.MainNotificationViewOperation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by izzuddiin on 3/28/17.
 */

public class NotificationPresenter implements MainNotificationPresenterOperation {
    private Context mContext;

    private MainNotificationViewOperation mMainView;

    private NotificationService mNotificationService;

    public NotificationPresenter(Context context, MainNotificationViewOperation mainView) {
        mContext = context;
        mMainView = mainView;
        mNotificationService = ApiUtils.getNotificationService();
    }

    @Override
    public void loadNotification() {
        String userId = PreferenceManager.getInstance(mContext).getId();
        String token = PreferenceManager.getInstance(mContext).getToken();

        mNotificationService.getAllNotification(userId, token).enqueue(new Callback<List<Notification>>() {
            @Override
            public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {
                if(response.isSuccessful()) {
                    mMainView.updateAdapter(response.body());
                } else {
                    Error mError = ApiUtils.parseError(response);
                    mMainView.showToast(mError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<Notification>> call, Throwable t) {
                mMainView.showToast(t.getMessage());
            }
        });
    }


}
