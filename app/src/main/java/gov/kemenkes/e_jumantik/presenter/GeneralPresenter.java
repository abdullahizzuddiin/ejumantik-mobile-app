package gov.kemenkes.e_jumantik.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import gov.kemenkes.e_jumantik.CoverActivity;
import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.model.District;
import gov.kemenkes.e_jumantik.model.Error;
import gov.kemenkes.e_jumantik.model.GeneralInfo;
import gov.kemenkes.e_jumantik.model.Province;
import gov.kemenkes.e_jumantik.model.Regency;
import gov.kemenkes.e_jumantik.model.User;
import gov.kemenkes.e_jumantik.model.Village;
import gov.kemenkes.e_jumantik.presenter_operation.CoverPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.LoginPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.MainPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.RegisterPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.ResetPasswordPresenterOperation;
import gov.kemenkes.e_jumantik.presenter_operation.SplashscreenPresenterOperation;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import gov.kemenkes.e_jumantik.rest_api.services.GeneralService;
import gov.kemenkes.e_jumantik.tool.Constant;
import gov.kemenkes.e_jumantik.tool.MyDatabase;
import gov.kemenkes.e_jumantik.tool.PreferenceManager;
import gov.kemenkes.e_jumantik.tool.Utilities;
import gov.kemenkes.e_jumantik.view_operation.CoverViewOperation;
import gov.kemenkes.e_jumantik.view_operation.LoginViewOperation;
import gov.kemenkes.e_jumantik.view_operation.MainViewOperation;
import gov.kemenkes.e_jumantik.view_operation.RegisterViewOperation;
import gov.kemenkes.e_jumantik.view_operation.ResetPasswordViewOperation;
import gov.kemenkes.e_jumantik.view_operation.SplashscreenViewOperation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by izzuddiin on 2/19/17.
 */

public class GeneralPresenter implements
        CoverPresenterOperation,
        RegisterPresenterOperation,
        LoginPresenterOperation,
        SplashscreenPresenterOperation,
        ResetPasswordPresenterOperation,
        MainPresenterOperation, FacebookCallback<LoginResult> {
    private GeneralService mGeneralService;
    private final String TAG = "FRONT PRESENTER";

    private Context mContext;

    private CoverViewOperation mCoverView;
    private RegisterViewOperation mRegisterView;
    private LoginViewOperation mLoginView;
    private SplashscreenViewOperation mSplashscreenView;
    private MainViewOperation mMainView;
    private ResetPasswordViewOperation mResetPasswordView;

    private CallbackManager mCallbackManager;

    private AccessToken mAccessToken;

    public GeneralPresenter(Context context, CoverViewOperation coverView) {
        mContext = context;
        mCoverView = coverView;
        mGeneralService = ApiUtils.getGeneralService();
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, this);
    }

    public GeneralPresenter(Context context, RegisterViewOperation registerView) {
        mContext = context;
        mRegisterView = registerView;
        mGeneralService = ApiUtils.getGeneralService();
    }

    public GeneralPresenter(Context context, LoginViewOperation loginView) {
        mContext = context;
        mLoginView = loginView;
        mGeneralService = ApiUtils.getGeneralService();
    }

    public GeneralPresenter(Context context, SplashscreenViewOperation splashscreenView) {
        mContext = context;
        mSplashscreenView = splashscreenView;
    }

    public GeneralPresenter(Context context, MainViewOperation mainView) {
        mContext = context;
        mMainView = mainView;
        mGeneralService = ApiUtils.getGeneralService();
    }

    public GeneralPresenter(Context context, ResetPasswordViewOperation resetPasswordView) {
        mContext = context;
        mResetPasswordView = resetPasswordView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_login_cover:
                mCoverView.navigateToLoginView();
                break;
            case R.id.btn_register_cover:
                mCoverView.navigateToRegisterView();
                break;
            case R.id.btn_login_facebook_cover:
                onLoginFacebookClicked();
                Log.d(TAG, "LOGIN FACEBOOK CLICKED");
                break;
            case R.id.btn_register:
                if(validateRegistrationForm()) {
                    onRegisterClicked();
                }
                break;
            case R.id.et_register_birth_date:
                mRegisterView.setBirthDateError(null);
                mRegisterView.showDatePicker();
                Log.d(TAG, "BIRTH DATE CLICKED");
                break;
            case R.id.btn_login:
                if(validateLoginForm())
                    onLoginClicked();
                break;
            case R.id.tv_login_forgot_password:
                mLoginView.navigateToResetPasswordView();
                break;
            case R.id.btn_reset:
                if(validateResetForm())
                    onResetClicked();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method needed because ordinary RadioGroup cannot detect
     * whether Rey Material RadioButton had checked
     * (explanation for first two cases)
     *
     * @param buttonView
     * @param isChecked
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int checkedId = buttonView.getId();

        switch (checkedId) {
            case R.id.rb_sex_male:
                mRegisterView.setFemaleRbChecked(!isChecked);
                mRegisterView.setSexError(null);
                break;
            case R.id.rb_sex_female:
                mRegisterView.setMaleRbChecked(!isChecked);
                mRegisterView.setSexError(null);
                break;
            case R.id.cb_show_password:
                if(isChecked)
                    mLoginView.showPassword(true);
                else
                    mLoginView.showPassword(false);
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        int selected_view_id = v.getId();
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            switch (selected_view_id) {
                case R.id.et_register_conf_password:
                    if(validateRegistrationForm())
                        onRegisterClicked();
                    break;
                case R.id.et_login_password:
                    if(validateLoginForm())
                        onLoginClicked();
                    break;
            }
        }
        return false;
    }

    @Override
    public void setupData() {
        if(PreferenceManager.getInstance(mContext).isFirstTimeUsed()) {
            loadJSONFromAssetFolder("provinces",null);
            loadJSONFromAssetFolder("regencies",null);
            loadJSONFromAssetFolder("districts",null);
            for (int ii = 1; ii <= 6; ii++) {
                loadJSONFromAssetFolder("villages",String.valueOf(ii));
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(PreferenceManager.getInstance(mContext).getId().equals("-1"))
                        mSplashscreenView.navigateToCoverView();
                    else
                        mSplashscreenView.navigateToMainView();
                }
            }, 1500);
        }
    }

    private void onLoginClicked() {
        login(mLoginView.getEmail(), mLoginView.getPassword());
    }

    private void onLoginFacebookClicked() {
        LoginManager.getInstance().logInWithReadPermissions((CoverActivity) mContext,
                Arrays.asList("user_photos", "email", "user_birthday", "public_profile"));
    }

    private void onRegisterClicked() {
        register(mRegisterView.getName(),mRegisterView.getEmail(), mRegisterView.getPassword(),
                mRegisterView.getBirthPlace(), mRegisterView.getBirthDate(), mRegisterView.getSex(),
                mRegisterView.getPhoneNumber());
    }

    private void onResetClicked() {
        resetPassword(mResetPasswordView.getEmail());
    }

    private boolean validateRegistrationForm() {
        boolean isFormValid = true;

        if (!Utilities.isFormFilled(mRegisterView.getName())) {
            isFormValid = false;
            mRegisterView.setNameError(Constant.MUST_BE_FILLED);
        }

        if (Utilities.isFormFilled(mRegisterView.getEmail())) {
            if (!Utilities.isEmailValid(mRegisterView.getEmail())) {
                isFormValid = false;
                mRegisterView.setEmailError(Constant.INVALID_EMAIL);
            }
        } else {
            isFormValid = false;
            mRegisterView.setEmailError(Constant.MUST_BE_FILLED);
        }

        if (!mRegisterView.isMaleRbChecked() && !mRegisterView.isFemaleRbChecked()) {
            isFormValid = false;
            mRegisterView.setSexError(Constant.MUST_BE_FILLED);
        }

        if (!Utilities.isFormFilled(mRegisterView.getBirthPlace())) {
            isFormValid = false;
            mRegisterView.setBirthPlaceError(Constant.MUST_BE_FILLED);
        }

        if (!Utilities.isFormFilled(mRegisterView.getBirthDate())) {
            isFormValid = false;
            mRegisterView.setBirthDateError(Constant.MUST_BE_FILLED);
        }

        if (Utilities.isFormFilled(mRegisterView.getPhoneNumber())) {
            if (!Utilities.isPhoneNumberValid(mRegisterView.getPhoneNumber())) {
                isFormValid = false;
                mRegisterView.setPhoneNumberError(Constant.INVALID_PHONE_NUMBER);
            }
        } else {
            isFormValid = false;
            mRegisterView.setPhoneNumberError(Constant.MUST_BE_FILLED);
        }

        if(Utilities.isFormFilled(mRegisterView.getPassword())) {
            if(!Utilities.isPasswordValid(mRegisterView.getPassword())) {
                isFormValid = false;
                mRegisterView.setPasswordError(Constant.MINIMUM_PASSWORD_NOT_SATISFIED);
            }
        } else {
            isFormValid = false;
            mRegisterView.setPasswordError(Constant.MUST_BE_FILLED);
        }

        if(Utilities.isFormFilled(mRegisterView.getConfPassword())) {
            if(!Utilities.isPasswordValid(mRegisterView.getConfPassword())) {
                isFormValid = false;
                mRegisterView.setConfPasswordError(Constant.MINIMUM_PASSWORD_NOT_SATISFIED);
            }
        } else {
            isFormValid = false;
            mRegisterView.setConfPasswordError(Constant.MUST_BE_FILLED);
        }

        if(!Utilities.isPasswordEqual(mRegisterView.getPassword(), mRegisterView.getConfPassword())) {
            isFormValid = false;
            mRegisterView.setConfPasswordError(Constant.PASSWORD_NOT_MATCH);
        }

        return isFormValid;
    }

    private boolean validateLoginForm() {
        boolean isAllTrue = true;

        if(Utilities.isFormFilled(mLoginView.getEmail())) {
            if(!Utilities.isEmailValid(mLoginView.getEmail())) {
                isAllTrue = false;
                mLoginView.setEmailError(Constant.INVALID_EMAIL);
            }
        } else {
            isAllTrue = false;
            mLoginView.setEmailError(Constant.MUST_BE_FILLED);
        }

        if(Utilities.isFormFilled(mLoginView.getPassword())) {
            if(!Utilities.isPasswordValid(mLoginView.getPassword())) {
                isAllTrue = false;
                mLoginView.setPasswordError(Constant.MINIMUM_PASSWORD_NOT_SATISFIED);
            }
        } else {
            isAllTrue = false;
            mLoginView.setPasswordError(Constant.MUST_BE_FILLED);
        }

        return isAllTrue;
    }

    private boolean validateResetForm() {
        if(Utilities.isFormFilled(mResetPasswordView.getEmail())) {
            if(!Utilities.isEmailValid(mResetPasswordView.getEmail())) {
                mResetPasswordView.setEmailError(Constant.INVALID_EMAIL);
                return false;
            } else {
                return true;
            }
        } else {
            mResetPasswordView.setEmailError(Constant.MUST_BE_FILLED);
            return false;
        }
    }

    private void login(String email, String password) {
        mGeneralService.login(email, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    User mUser = response.body();
                    PreferenceManager.getInstance(mContext).saveProfile(
                            mUser.getId(), mUser.getToken(), mUser.getFullname(), mUser.getEmail(),
                            mUser.getBirthPlace(), mUser.getBirthDate(), mUser.getSex(), mUser.getPhoneNumber(),
                            mUser.getPoint());
                    mLoginView.navigateToMainView();
                } else {
                    Error mError = ApiUtils.parseError(response);
                    mLoginView.showToast(mError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                mLoginView.showToast(t.getMessage());
            }
        });

    }

    private void loginFacebook(String userId) {
        mGeneralService.loginFacebook(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    User mUser = response.body();
                    PreferenceManager.getInstance(mContext).saveProfile(
                            mUser.getId(), mUser.getToken(), mUser.getFullname(), mUser.getEmail(),
                            mUser.getBirthPlace(), mUser.getBirthDate(), mUser.getSex(), mUser.getPhoneNumber(),
                            mUser.getPoint());
                    mCoverView.navigateToMainView();
                } else if (response.code() == ApiUtils.FACEBOOK_LOGIN_FAILED) {
                    mCoverView.showToast("Mengambil data Facebook");
                    GraphRequest request = GraphRequest.newMeRequest(mAccessToken, new GraphRequest.GraphJSONObjectCallback() {

                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Get facebook data from login
                            Bundle facebookBundle = convertFacebookResponse(object,mAccessToken.getUserId());
                            if(facebookBundle != null) {
                                mCoverView.dismissLoadingDialog();
                                mCoverView.navigateToRegisterView(facebookBundle);
                            }
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                    request.setParameters(parameters);
                    request.executeAsync();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                mCoverView.showToast(t.getMessage());
            }
        });
    }

    @Override
    public void logout() {
        String email = PreferenceManager.getInstance(mContext).getEmail();
        String token = PreferenceManager.getInstance(mContext).getToken();
        mGeneralService.logout(email, token).enqueue(new Callback<GeneralInfo>() {
            @Override
            public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                if(response.isSuccessful()) {
                    PreferenceManager.getInstance(mContext).clearProfile();
                    mMainView.navigateToCoverView();
                }  else {
                    Error mError = ApiUtils.parseError(response);
                    mMainView.showToast(mError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GeneralInfo> call, Throwable t) {
                mMainView.showToast(t.getMessage());
            }
        });
    }

    private void register(String fullname, String email, String password,
                          String birthPlace, String birthDate, boolean sex, String phoneNumber) {
        mRegisterView.showToast("Mendaftarkan Pengguna");
        User newUser = new User();
        newUser.setToken("");
        newUser.setFullname(fullname);
        newUser.setEmail(email);
        newUser.setPassword(password);
        newUser.setBirthPlace(birthPlace);
        newUser.setBirthDate(birthDate);
        newUser.setSex(Utilities.parseSex(sex));
        newUser.setPhoneNumber(phoneNumber);
        newUser.setPoint(0);

        if(mRegisterView.getFacebookId() == null)
            mGeneralService.register(newUser).enqueue(new Callback<GeneralInfo>() {
            @Override
            public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                if(response.isSuccessful()) {
                    mRegisterView.showToast("Pendaftaran Berhasil");
                    mRegisterView.navigateToCoverView();
                } else {
                    Error mError = ApiUtils.parseError(response);
                    mRegisterView.showToast(mError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GeneralInfo> call, Throwable t) {
                mRegisterView.showToast(t.getMessage());
            }
        });
        else
            mGeneralService.registerWithFacebook(fullname, email, password, birthPlace,
                    birthDate, Utilities.parseSex(sex), phoneNumber, 0,
                    mRegisterView.getFacebookId()).enqueue(new Callback<GeneralInfo>() {
                @Override
                public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                    if(response.isSuccessful()) {
                        mRegisterView.showToast("Pendaftaran Berhasil");
                        mRegisterView.navigateToCoverView();
                    } else {
                        Error mError = ApiUtils.parseError(response);
                        mRegisterView.showToast(mError.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<GeneralInfo> call, Throwable t) {
                    mRegisterView.showToast(t.getMessage());
                }
            });

    }

    private void resetPassword(final String email) {
        mGeneralService.resetPassword(email).enqueue(new Callback<GeneralInfo>() {
            @Override
            public void onResponse(Call<GeneralInfo> call, Response<GeneralInfo> response) {
                // TODO: 4/6/17 show toast aja
                if(response.isSuccessful()) {
                    mResetPasswordView.showSuccessDialog(email);
                } else {
                    Error mError = ApiUtils.parseError(response);
                    mResetPasswordView.showToast(mError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GeneralInfo> call, Throwable t) {
                //// TODO: 4/6/17 show toast
                mResetPasswordView.showToast(t.getMessage());
            }
        });
    }

    public Bundle convertFacebookResponse(JSONObject object, String facebookId) {
        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

//            try {
//                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
//                Log.i("profile_pic", profile_pic + "");
//                bundle.putString("profile_pic", profile_pic.toString());
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                return null;
//            }

            bundle.putString("facebookId", facebookId);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

            return bundle;
        }
        catch(JSONException e) {
            Log.d(TAG,"Error parsing JSON");
        }
        return null;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        mAccessToken = loginResult.getAccessToken();
        mCoverView.showLoadingDialog();
        loginFacebook(mAccessToken.getUserId());
    }

    @Override
    public void onCancel() {
        mCoverView.showToast("Batal Login Facebook");
    }

    @Override
    public void onError(FacebookException error) {
        mCoverView.showToast(error.getMessage());
    }

    //region JSON LOAD
    private void loadJSONFromAssetFolder(String jsonName, String extension) {
        String json;
        try {
            String jsonFileName = "";
            if(extension == null)
                jsonFileName = jsonName+".json";
            else
                jsonFileName = jsonName+"_"+extension+".json";


            InputStream is = mContext.getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            switch (jsonName) {
                case "provinces":
                    readProvinceJson(json);
                    break;
                case "regencies":
                    readRegencyJson(json);
                    break;
                case "districts":
                    readDistrictJson(json);
                    break;
                case "villages":
                    readVillageJson(json, extension);
                    break;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readVillageJson(final String json, final String extension) {
        FlowManager.getDatabase(MyDatabase.class).beginTransactionAsync(
                new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        try {
                            JSONArray raw = new JSONArray(json);
                            for(int ii=0; ii<raw.length();ii++) {
                                JSONObject regencyJson = raw.getJSONObject(ii);
                                Village item = new Village();
                                item.setId(regencyJson.getString("id"));
                                item.setName(regencyJson.getString("name"));
                                item.setDistrictId(regencyJson.getString("district_id"));
                                item.save();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).error(new Transaction.Error() {
            @Override
            public void onError(Transaction transaction, Throwable error) {
                Log.d("TAG","VILLAGE ERROR " + extension);
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                mSplashscreenView.setProgressViewValue((Integer.parseInt(extension)+3)*11);
                if(extension.equals("6")) {
                    mSplashscreenView.navigateToCoverView();
                    PreferenceManager.getInstance(mContext).setFirstTimeUsed(false);
                }
                Log.d("TAG","VILLAGE SUCCESS "+extension);
            }
        }).build().execute();
    }

    private void readDistrictJson(final String json) {
        FlowManager.getDatabase(MyDatabase.class).beginTransactionAsync(
                new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        try {
                            JSONArray raw = new JSONArray(json);
                            for(int ii=0; ii<raw.length();ii++) {
                                JSONObject regencyJson = raw.getJSONObject(ii);
                                District item = new District();
                                item.setId(regencyJson.getString("id"));
                                item.setName(regencyJson.getString("name"));
                                item.setRegencyId(regencyJson.getString("regency_id"));
                                item.save();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).error(new Transaction.Error() {
            @Override
            public void onError(Transaction transaction, Throwable error) {
                Log.d("TAG","DISTRICT ERROR");
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                mSplashscreenView.setProgressViewValue(33);
                Log.d("TAG","DISTRCT SUCCESS");
            }
        }).build().execute();
    }

    private void readRegencyJson(final String json) {
        FlowManager.getDatabase(MyDatabase.class).beginTransactionAsync(
                new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        try {
                            JSONArray raw = new JSONArray(json);
                            for(int ii=0; ii<raw.length();ii++) {
                                JSONObject regencyJson = raw.getJSONObject(ii);
                                Regency item = new Regency();
                                item.setId(regencyJson.getString("id"));
                                item.setName(regencyJson.getString("name"));
                                item.setProvinceId(regencyJson.getString("province_id"));
                                item.save();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).error(new Transaction.Error() {
            @Override
            public void onError(Transaction transaction, Throwable error) {
                Log.d("TAG","REGENCY ERROR");
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                mSplashscreenView.setProgressViewValue(22);
                Log.d("TAG","REGENCY SUCCESS");
            }
        }).build().execute();
    }

    private void readProvinceJson(final String json) {
        FlowManager.getDatabase(MyDatabase.class).beginTransactionAsync(
                new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        try {
                            JSONArray raw = new JSONArray(json);
                            for(int ii=0; ii<raw.length();ii++) {
                                JSONObject provinceJson = raw.getJSONObject(ii);
                                Province item = new Province();
                                item.setId(provinceJson.getString("id"));
                                item.setName(provinceJson.getString("name"));
                                item.save();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).error(new Transaction.Error() {
            @Override
            public void onError(Transaction transaction, Throwable error) {
                Log.d("TAG","PROVINCE ERROR");
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(Transaction transaction) {
                mSplashscreenView.setProgressViewValue(11);
                Log.d("TAG","PROVINCE SUCCESS");
            }
        }).build().execute();
    }
    //endregion
}
