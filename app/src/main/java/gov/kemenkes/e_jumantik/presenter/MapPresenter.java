package gov.kemenkes.e_jumantik.presenter;

import android.content.Context;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.presenter_operation.MapPresenterOperation;
import gov.kemenkes.e_jumantik.view_operation.MapViewOperation;

/**
 * Created by izzuddiin on 3/28/17.
 */

public class MapPresenter implements MapPresenterOperation {
    private Context mContext;
    private MapViewOperation mMapView;

    private LatLng mLastPosition;

    public MapPresenter(Context context, MapViewOperation mapView) {
        mContext = context;
        mMapView = mapView;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mLastPosition = latLng;
        mMapView.clearMap();
        mMapView.placeMarker(latLng);
        mMapView.moveCamera(latLng);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_next_map:
                if(mLastPosition!=null)
                    mMapView.navigateToObservingInfoView(mLastPosition);
                else
                    mMapView.showToast("Pilih Lokasi Pemantauan");
                break;
        }
    }
}
