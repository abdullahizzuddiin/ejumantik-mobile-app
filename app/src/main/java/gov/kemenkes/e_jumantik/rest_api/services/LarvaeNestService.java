package gov.kemenkes.e_jumantik.rest_api.services;

import java.util.List;
import java.util.Map;

import gov.kemenkes.e_jumantik.model.GeneralInfo;
import gov.kemenkes.e_jumantik.model.LarvaeNest;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by izzuddiin on 3/26/17.
 */

public interface LarvaeNestService {
    @GET(ApiUtils.LARVAE_NEST)
    Call<List<LarvaeNest>> getAllLarvae(@Query("id") String user_id,
                                        @Query("token") String token);

    @Multipart
    @POST("tes")
    Call<GeneralInfo> uploadTestPictre(@Part MultipartBody.Part foto_before,
                                       @Part MultipartBody.Part foto_after);

    @Multipart
    @POST(ApiUtils.CREATE_LARVAE_NEST)
    Call<GeneralInfo> createLarvae(@PartMap() Map<String, RequestBody> partMap,
                                   @Part MultipartBody.Part foto_before,
                                   @Part MultipartBody.Part foto_after);

    @Multipart
    @POST(ApiUtils.UPDATE_LARVAE_NEST)
    Call<GeneralInfo> updateLarvae(@PartMap() Map<String, RequestBody> partMap,
                                   @Part MultipartBody.Part foto_before,
                                   @Part MultipartBody.Part foto_after);

    @Multipart
    @POST(ApiUtils.UPDATE_LARVAE_NEST)
    Call<GeneralInfo> updateLarvaeParcial(@PartMap() Map<String, RequestBody> partMap,
                                   @Part MultipartBody.Part foto);

    @Multipart
    @POST(ApiUtils.UPDATE_LARVAE_NEST)
    Call<GeneralInfo> updateLarvaeEmpty(@PartMap() Map<String, RequestBody> partMap);

    @DELETE(ApiUtils.DELETE_LARVAE_NEST)
    Call<GeneralInfo> deleteLarvae(@Path("larvae_nest") String id,
                                   @Field("token") String token);

}
