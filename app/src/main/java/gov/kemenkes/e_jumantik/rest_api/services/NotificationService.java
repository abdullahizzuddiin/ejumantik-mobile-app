package gov.kemenkes.e_jumantik.rest_api.services;

import java.util.List;

import gov.kemenkes.e_jumantik.model.GeneralInfo;
import gov.kemenkes.e_jumantik.model.Notification;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by izzuddiin on 3/26/17.
 */

public interface NotificationService {
    @GET(ApiUtils.NOTIFICATION)
    Call<List<Notification>> getAllNotification(@Query("id") String user_id,
                                                @Query("token") String token);

    @PUT(ApiUtils.UPDATE_SEEN_NOTIFICATION)
    @FormUrlEncoded
    Call<GeneralInfo> updateSeenNotification(@Path("notification") String id,
                                             @Field("token") String token);
}
