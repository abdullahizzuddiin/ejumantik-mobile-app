package gov.kemenkes.e_jumantik.rest_api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import gov.kemenkes.e_jumantik.model.Error;
import gov.kemenkes.e_jumantik.rest_api.services.GeneralService;
import gov.kemenkes.e_jumantik.rest_api.services.LarvaeNestService;
import gov.kemenkes.e_jumantik.rest_api.services.NotificationService;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by izzuddiin on 3/24/17.
 */

public class ApiUtils {
    public static final String BASE_URL = "http://pokentik.com/api/";
    public static final String IMAGE_URL = "http://pokentik.com/foto/";
    public static final String LOGIN = "login";
    public static final String LOGIN_FACEBOOK =  "tesfb";
    public static final String RESET_PASSWORD = "reset";
    public static final String REGISTRATION = "userstore";
    public static final String LOGOUT = "logout";
    //GET ALL, CREATE, DELETE, UPDATE
    public static final String LARVAE_NEST = "getobserve";
    public static final String CREATE_LARVAE_NEST = "larvaestore";
    public static final String UPDATE_LARVAE_NEST = "larvaeupdate";
    public static final String DELETE_LARVAE_NEST = LARVAE_NEST + "{larvae_nest}";
    //GET ALL, UPDATE
    public static final String NOTIFICATION = "getnotif";
    public static final String UPDATE_NOTIFICATION = NOTIFICATION + "{notification}";
    public static final String UPDATE_SEEN_NOTIFICATION = NOTIFICATION + "{notification}";

    public static final int FACEBOOK_LOGIN_FAILED = 300;

    public static GeneralService getGeneralService() {
        return RetrofitClient.getClient(BASE_URL).create(GeneralService.class);
    }

    public static LarvaeNestService getLarvaeNestService() {
        return RetrofitClient.getClient(BASE_URL).create(LarvaeNestService.class);
    }

    public static NotificationService getNotificationService() {
        return RetrofitClient.getClient(BASE_URL).create(NotificationService.class);
    }

    public static Converter<ResponseBody, Error> getErrorConverter() {
        return RetrofitClient.getClient(BASE_URL).responseBodyConverter(Error.class, new Annotation[0]);
    }

    public static Error parseError(Response<?> response) {
        Converter<ResponseBody, Error> converter = getErrorConverter();

        Error mError;
        try {
            mError = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new Error(response.code(),"Unknown Error");
        }

        return mError;
    }
}
