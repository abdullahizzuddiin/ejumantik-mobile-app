package gov.kemenkes.e_jumantik.rest_api.services;

import gov.kemenkes.e_jumantik.model.GeneralInfo;
import gov.kemenkes.e_jumantik.model.User;
import gov.kemenkes.e_jumantik.rest_api.ApiUtils;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by izzuddiin on 3/24/17.
 */

public interface GeneralService {
    //Login
    @POST(ApiUtils.LOGIN)
    @FormUrlEncoded
    Call<User> login(@Field("email") String email,
                     @Field("password") String password);

    //Login Facebook
    @POST(ApiUtils.LOGIN_FACEBOOK)
    @FormUrlEncoded
    Call<User> loginFacebook(@Field("facebook_id") String facebook_id);

    //Logout
    @POST(ApiUtils.LOGOUT)
    @FormUrlEncoded
    Call<GeneralInfo> logout(@Field("email") String email,
                             @Field("token") String token);

    //Reset Password
    @POST(ApiUtils.RESET_PASSWORD)
    @FormUrlEncoded
    Call<GeneralInfo> resetPassword(@Field("email") String email);

    //Registration
    @POST(ApiUtils.REGISTRATION)
    Call<GeneralInfo> register(@Body User user);

    //Registration
    @FormUrlEncoded
    @POST(ApiUtils.REGISTRATION)
    Call<GeneralInfo> registerWithFacebook(@Field("name") String fullname,
                                           @Field("email") String email,
                                           @Field("password") String password,
                                           @Field("birth_place") String birthPlace,
                                           @Field("birth_date") String birthDate,
                                           @Field("sex") int sex,
                                           @Field("phone_number") String phoneNumber,
                                           @Field("point") int point,
                                           @Field("facebook_id") String facebookId);
}
