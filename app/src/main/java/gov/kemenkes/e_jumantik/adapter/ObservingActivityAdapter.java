package gov.kemenkes.e_jumantik.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import gov.kemenkes.e_jumantik.DetailObservingActivity;
import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.model.LarvaeNest;
import gov.kemenkes.e_jumantik.tool.ImageHelper;
import gov.kemenkes.e_jumantik.view_operation.MainObservingActivityAdapterViewOperation;

/**
 * Created by izzuddiin on 3/4/17.
 */

public class ObservingActivityAdapter
        extends RecyclerView.Adapter<ObservingActivityAdapter.ViewHolder>
        implements View.OnClickListener {
    private Context mContext;
    private List<LarvaeNest> mLarvaeNests;
    private LayoutInflater mInflater;
    private MainObservingActivityAdapterViewOperation mMainView;

    public ObservingActivityAdapter(Context context, List<LarvaeNest> larvaeNests) {
        mContext = context;
        mLarvaeNests = larvaeNests;
        mInflater = LayoutInflater.from(mContext);
    }

    public void setMainView(MainObservingActivityAdapterViewOperation mainView) {
        mMainView = mainView;
    }

    public void updateObservingActivity(List<LarvaeNest> items) {
        mLarvaeNests = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_observing_activity_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final LarvaeNest mLarvaeNest = mLarvaeNests.get(position);
        holder.addressTv.setText(mContext.getResources().getString(
                R.string.address_text,mLarvaeNest.getAddress(),mLarvaeNest.getVillageId(),
                mLarvaeNest.getDistrictId(), mLarvaeNest.getRegencyId(), mLarvaeNest.getProvinceId()));

        File beforeImageFile = ImageHelper.getImageLocal(mLarvaeNest.getBeforeImage());
        setupImage(holder.beforeObserveIv, ImageHelper.getImageUrl(mLarvaeNest.getBeforeImage()), beforeImageFile);

        File afterImageFile = ImageHelper.getImageLocal(mLarvaeNest.getAfterImage());
        setupImage(holder.afterObserveIv, ImageHelper.getImageUrl(mLarvaeNest.getAfterImage()), afterImageFile);

        holder.detailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailObservingActivity.class);
                intent.putExtra("larvae_nest_id", mLarvaeNest.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
        holder.shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    File newImage = ImageHelper.createImageFile();
                    OutputStream os = new FileOutputStream(newImage);
                    holder.containerRl.setDrawingCacheEnabled(true);
                    Bitmap b1 = holder.containerRl.getDrawingCache();
                    Bitmap b = b1.copy(Bitmap.Config.ARGB_8888, false);
                    b.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.flush();
                    os.close();
                    holder.containerRl.destroyDrawingCache();
                    mMainView.shareImage(newImage, mLarvaeNest);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setupImage(ImageView imageView, String url, File file) {
        if(file != null)
            Picasso.with(mContext).load(file).
                    placeholder(R.drawable.ic_crop_original_black).config(Bitmap.Config.RGB_565).fit().
                    into(imageView);
        else if(url != null)
            Picasso.with(mContext).load(url).
                    placeholder(R.drawable.ic_crop_original_black).config(Bitmap.Config.RGB_565).fit().
                    into(imageView);
    }

    @Override
    public int getItemCount() {
        return mLarvaeNests.size();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_share_observe_activity:
                Toast.makeText(mContext, "Fitur masih dikembangkan", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout containerRl;
        ImageView beforeObserveIv;
        ImageView afterObserveIv;
        TextView addressTv;
        Button shareBtn;
        Button detailBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            containerRl = (RelativeLayout) itemView.findViewById(R.id.rl_container);
            beforeObserveIv = (ImageView) itemView.findViewById(R.id.before_observe_row_pic_iv);
            afterObserveIv = (ImageView) itemView.findViewById(R.id.after_observe_row_pic_iv);
            addressTv = (TextView) itemView.findViewById(R.id.tv_address_activity);
            shareBtn = (Button) itemView.findViewById(R.id.btn_share_observe_activity);
            detailBtn = (Button) itemView.findViewById(R.id.btn_detail_observe_activity);
        }
    }
}
