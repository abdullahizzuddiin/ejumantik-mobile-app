package gov.kemenkes.e_jumantik.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.model.Notification;

/**
 * Created by izzuddiin on 3/5/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context mContext;
    private List<Notification> mNotifications;
    private LayoutInflater mInflater;

    public NotificationAdapter(Context context, List<Notification> notifications) {
        mContext = context;
        mNotifications = notifications;
        mInflater = LayoutInflater.from(mContext);
    }

    public void updateNotification(List<Notification> items) {
        mNotifications = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_notification_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Notification mNotification = mNotifications.get(position);
        holder.titleTv.setText(mNotification.getTitle());
        holder.contentTv.setText(mNotification.getContent());
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTv;
        TextView contentTv;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTv = (TextView) itemView.findViewById(R.id.tv_notification_title);
            contentTv = (TextView) itemView.findViewById(R.id.tv_notification_content);
        }
    }
}
