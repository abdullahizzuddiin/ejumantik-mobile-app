package gov.kemenkes.e_jumantik.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import gov.kemenkes.e_jumantik.R;
import gov.kemenkes.e_jumantik.item.NavDrawerItem;
import gov.kemenkes.e_jumantik.listener.ViewHolderClickListener;
import gov.kemenkes.e_jumantik.view_operation.MainViewOperation;

/**
 * Created by izzuddiin on 2/26/17.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {
    private List<NavDrawerItem> mItemLlist;
    private LayoutInflater mInflater;
    private Context mContext;
    private MainViewOperation mMainView;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> itemList) {
        mContext = context;
        mItemLlist = itemList;
        mInflater = LayoutInflater.from(context);
    }

    public void initiateViewOperation(MainViewOperation mainView) {
        mMainView = mainView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_navigation_drawer, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NavDrawerItem item = mItemLlist.get(position);
        Picasso.with(mContext).load(item.getIcon()).config(Bitmap.Config.RGB_565).into(holder.icon);
        holder.title.setText(item.getTitle());
        holder.setClickListener(new ViewHolderClickListener() {
            @Override
            public void onClickRecyclerView(View v, int position, boolean isLongClick) {
                switch (position) {
                    case 0:
                        mMainView.navigateToMosquitoView();
                        break;
                    case 1:
                        mMainView.navigateToPointView();
                        break;
                    case 2:
                        mMainView.navigateToNotificationView();
                        break;
                    case 3:
                        onSignOutClicked();
                        break;
                }
            }
        });
    }

    private void onSignOutClicked() {
        mMainView.goLogout();
    }

    @Override
    public int getItemCount() {
        return mItemLlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView icon;
        ViewHolderClickListener listener;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.nav_drawer_title);
            icon = (ImageView) itemView.findViewById(R.id.nav_drawer_icon);
            itemView.setOnClickListener(this);
        }

        public void setClickListener(ViewHolderClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onClickRecyclerView(v, getPosition(),false);
        }
    }
}
