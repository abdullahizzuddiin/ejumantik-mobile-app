package gov.kemenkes.e_jumantik.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.transaction.FastStoreModelTransaction;

import java.util.List;

import gov.kemenkes.e_jumantik.tool.MyDatabase;

/**
 * Created by izzuddiin on 3/4/17.
 */
@Table(database = MyDatabase.class)
public class LarvaeNest extends BaseModel{
    @Column
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("userId")
    @Expose
    @Column(defaultValue = "")
    private String userId;

    @SerializedName("country")
    @Expose
    @Column(defaultValue = "")
    private String countryId;

    @SerializedName("province")
    @Expose
    @Column(defaultValue = "")
    private String provinceId;

    @SerializedName("city")
    @Expose
    @Column(defaultValue = "")
    private String regencyId;

    @SerializedName("district")
    @Expose
    @Column(defaultValue = "")
    private String districtId;

    @SerializedName("village")
    @Expose
    @Column(defaultValue = "")
    private String villageId;

    @SerializedName("address")
    @Expose
    @Column(defaultValue = "")
    private String address;

    @SerializedName("rt")
    @Expose
    @Column(defaultValue = "")
    private String rt;

    @SerializedName("rw")
    @Expose
    @Column(defaultValue = "")
    private String rw;

    @SerializedName("place_category")
    @Expose
    @Column(defaultValue = "0")
    private int placeCategory;

    @SerializedName("place_type")
    @Expose
    @Column(defaultValue = "0")
    private int placeType;

    @SerializedName("container_category")
    @Expose
    @Column(defaultValue = "0")
    private int containerType;

    @SerializedName("num_container")
    @Expose
    @Column(defaultValue = "0")
    private int numContainer;

    @SerializedName("any_larvae")
    @Expose
    @Column(defaultValue = "0")
    private int anyLarvae;

    @SerializedName("observing_date")
    @Expose
    @Column(defaultValue = "1970-01-01")
    private String observingDate;

    @SerializedName("latitude")
    @Expose
    @Column(defaultValue = "0")
    private float latitude;

    @SerializedName("longitude")
    @Expose
    @Column(defaultValue = "0")
    private float longitude;

    @SerializedName("before_image_path")
    @Expose
    @Column(defaultValue = "")
    private String beforeImage;

    @SerializedName("after_image_path")
    @Expose
    @Column(defaultValue = "")
    private String afterImage;

    @SerializedName("created_at")
    @Expose
    @Column(defaultValue = "")
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    @Column(defaultValue = "")
    private String updateddAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getRegencyId() {
        return regencyId;
    }

    public void setRegencyId(String cityId) {
        this.regencyId = cityId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public int getPlaceCategory() {
        return placeCategory;
    }

    public void setPlaceCategory(int placeCategory) {
        this.placeCategory = placeCategory;
    }

    public int getPlaceType() {
        return placeType;
    }

    public void setPlaceType(int placeType) {
        this.placeType = placeType;
    }

    public int getContainerType() {
        return containerType;
    }

    public void setContainerType(int containerType) {
        this.containerType = containerType;
    }

    public int getNumContainer() {
        return numContainer;
    }

    public void setNumContainer(int numContainer) {
        this.numContainer = numContainer;
    }

    public String getObservingDate() {
        return observingDate;
    }

    public void setObservingDate(String observingDate) {
        this.observingDate = observingDate;
    }

    public int getAnyLarvae() {
        return anyLarvae;
    }

    public void setAnyLarvae(int anyLarvae) {
        this.anyLarvae = anyLarvae;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getBeforeImage() {
        return beforeImage;
    }

    public void setBeforeImage(String beforeImagePath) {
        this.beforeImage = beforeImagePath;
    }

    public String getAfterImage() {
        return afterImage;
    }

    public void setAfterImage(String afterImagePath) {
        this.afterImage = afterImagePath;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateddAt() {
        return updateddAt;
    }

    public void setUpdateddAt(String updateddAt) {
        this.updateddAt = updateddAt;
    }

    public static void saveListLarvaeNest(List<LarvaeNest> list) {
        FlowManager.getDatabase(MyDatabase.class).executeTransaction(FastStoreModelTransaction
                .saveBuilder(FlowManager.getModelAdapter(LarvaeNest.class))
                .addAll(list)
                .build());
    }
}
