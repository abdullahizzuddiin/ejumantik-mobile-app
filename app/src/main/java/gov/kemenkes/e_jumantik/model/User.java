package gov.kemenkes.e_jumantik.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.structure.BaseModel;

import gov.kemenkes.e_jumantik.tool.MyDatabase;

/**
 * Created by izzuddiin on 3/1/17.
 */

@Table(database = MyDatabase.class)
public class User extends BaseModel{
    @Column
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("token")
    @Expose
    @Column(defaultValue = "")
    private String token;

    @SerializedName("name")
    @Expose
    @Column(defaultValue = "")
    private String fullname;

    @SerializedName("email")
    @Expose
    @Column(defaultValue = "")
    @Unique
    private String email;

    @SerializedName("password")
    @Expose
    @Column(defaultValue = "")
    private String password;

    @SerializedName("birth_place")
    @Expose
    @Column(defaultValue = "")
    private String birthPlace;

    @SerializedName("birth_date")
    @Expose
    @Column(defaultValue = "")
    private String birthDate;

    @SerializedName("sex")
    @Expose
    @Column(defaultValue = "0")
    private int sex;

    @SerializedName("phone_number")
    @Expose
    @Column(defaultValue = "000000000000")
    private String phoneNumber;

    @SerializedName("point")
    @Expose
    @Column(defaultValue = "0")
    private int point;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
