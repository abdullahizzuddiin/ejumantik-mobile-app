package gov.kemenkes.e_jumantik.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import gov.kemenkes.e_jumantik.tool.MyDatabase;

/**
 * Created by izzuddiin on 3/5/17.
 */

@Table(database = MyDatabase.class)
public class Notification extends BaseModel {
    @Column
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    @Column(defaultValue = "")
    private String userId;

    @SerializedName("title")
    @Expose
    @Column(defaultValue = "")
    private String title;

    @SerializedName("content")
    @Expose
    @Column(defaultValue = "")
    private String content;

    @SerializedName("type")
    @Expose
    @Column(defaultValue = "0")
    private int type;

    @SerializedName("seen")
    @Expose
    @Column(defaultValue = "0")
    private int seen;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }
}
