package gov.kemenkes.e_jumantik.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import gov.kemenkes.e_jumantik.tool.MyDatabase;

/**
 * Created by izzuddiin on 3/16/17.
 */

@Table(database = MyDatabase.class)
public class Regency extends BaseModel {
    @Column
    @PrimaryKey
    private String id;

    @Column
    private String provinceId;

    @Column
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
