package gov.kemenkes.e_jumantik;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by izzuddiin on 4/24/17.
 */

public class AlarmReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "NERIMA NIH");
        createNotification(context);
    }

    public void createNotification(Context context) {
        Log.d("TAG", "BENTAR LAGI PASANG NOTIF");
        Intent resultIntent = new Intent(context, SplashscreenActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.logo_circle)
                        .setContentTitle("Jangan Lupa!")
                        .setContentText("Bantu kami berantas sarang nyamuk di sekitar anda dengan...");

        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("Jangan Lupa!");
        inboxStyle.addLine("Bantu kami berantas sarang nyamuk di sekitar anda dengan aplikasi Pokentik.");
        inboxStyle.addLine("Satu Aktivitas yang anda lakukan akan menambah 10 poin.");
        inboxStyle.addLine("#TurnBackZika");

        mBuilder.setStyle(inboxStyle);
        // Sets an ID for the notification
        int mNotificationId = 001;

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
